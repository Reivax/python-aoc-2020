if __name__ == "__main__":
    with open("input.txt", "r") as f:
        correct = 0
        for line in f:
            tmin, rest = line.split('-', 1)
            tmax, rest = rest.split(' ', 1)
            t, rest = rest.split(':', 1)
            pwd = rest.strip()
            tmin = int(tmin)
            tmax = int(tmax)
            count = 0
            for c in pwd:
                if c == t:
                    count += 1
            if tmin <= count <= tmax:
                correct += 1
        print("Correct", correct)
