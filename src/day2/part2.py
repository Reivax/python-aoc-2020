if __name__ == "__main__":
    with open("input.txt", "r") as f:
        correct = 0
        for line in f:
            tl, rest = line.split('-', 1)
            tr, rest = rest.split(' ', 1)
            t, rest = rest.split(':', 1)
            pwd = rest.strip()
            tl = int(tl) - 1
            tr = int(tr) - 1
            if bool(pwd[tl] == t) ^ bool(pwd[tr] == t):
                correct += 1
        print("Correct", correct)
