import functools
def load(input_file):
    parsed = []
    with open(input_file) as f:
        for line in f:
            row = parse_line(line)
            # print(row)
            parsed.append(row)
    # print(parsed)
    return parsed

def parse_line(line):
    parsed_line = line[0], int(line[1:])
    return parsed_line

# headings = ['N', 'E', 'S', 'W']
headings = "NESW"
rev_headings = [*reversed(headings)]

def rotate(current_heading, turn_direction, turn_amt):
    print("Rotate:", current_heading, turn_direction, turn_amt)
    turn_idx = int(turn_amt/90)
    if turn_direction == 'R':
        current_idx = headings.index(current_heading)
        new_heading = headings[(current_idx + turn_idx) % 4]
    elif turn_direction == 'L':
        current_idx = rev_headings.index(current_heading)
        new_heading = rev_headings[(current_idx + turn_idx) % 4]
    else:
        assert False

    print("Rotated:", new_heading, turn_direction, turn_amt)
    return new_heading

def part1(input):
    ew = 0
    ns = 0
    heading = 'E'
    for (action, amt) in input:
        print("EW, NS", ew, ns)
        print("Action, Amt", action, amt)
        if action == 'F':
            print("Switching F for ", heading)
            action = heading
        if action == 'E':
            ew += amt
        elif action == 'W':
            ew -= amt
        elif action == 'N':
            ns += amt
        elif action == 'S':
            ns -= amt
        elif action in  ['L', 'R']:
            heading = rotate(heading, action, amt)
        else:
            assert False
    print("EW, NS", ew, ns)
    return abs(ew)+abs(ns)

if __name__ == "__main__":
    parsed_input = load("test.txt")
    p1 = part1(parsed_input)
    print(p1)
    assert p1==25

    parsed_input = load("input.txt")
    p1 = part1(parsed_input)
    print(p1)
    assert p1==1601
