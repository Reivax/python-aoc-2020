import functools
def load(input_file):
    parsed = []
    with open(input_file) as f:
        for line in f:
            row = parse_line(line)
            # print(row)
            parsed.append(row)
    # print(parsed)
    return parsed

def parse_line(line):
    parsed_line = line[0], int(line[1:])
    return parsed_line

def rotate(current_heading, turn_direction, turn_amt):
    print("Rotate:", current_heading, turn_direction, turn_amt)
    while turn_amt > 0:
        if turn_direction == 'R':
            if current_heading == 'E':
                current_heading = 'S'
            elif current_heading == 'S':
                current_heading = 'W'
            elif current_heading == 'W':
                current_heading = 'N'
            elif current_heading == 'N':
                current_heading = 'E'
            else:
                assert False
        elif turn_direction == 'L':
            if current_heading == 'E':
                current_heading = 'N'
            elif current_heading == 'S':
                current_heading = 'E'
            elif current_heading == 'W':
                current_heading = 'S'
            elif current_heading == 'N':
                current_heading = 'W'
            else:
                assert False
        else:
            assert False
        turn_amt -= 90
    print("Rotated:", current_heading, turn_direction, turn_amt)
    return current_heading

def part1(input):
    ew = 0
    ns = 0
    heading = 'E'
    for (action, amt) in input:
        print("EW, NS", ew, ns)
        print("Action, Amt", action, amt)
        if action == 'F':
            print("Switching F for ", heading)
            action = heading
        if action == 'E':
            ew += amt
        elif action == 'W':
            ew -= amt
        elif action == 'N':
            ns += amt
        elif action == 'S':
            ns -= amt
        elif action in  ['L', 'R']:
            heading = rotate(heading, action, amt)
        else:
            assert False
    print("EW, NS", ew, ns)
    return abs(ew)+abs(ns)

if __name__ == "__main__":
    parsed_input = load("test.txt")
    p1 = part1(parsed_input)
    print(p1)

    parsed_input = load("input.txt")
    p1 = part1(parsed_input)
    print(p1)
