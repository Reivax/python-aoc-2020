import functools
def load(input_file):
    parsed = []
    with open(input_file) as f:
        for line in f:
            row = parse_line(line)
            # print(row)
            parsed.append(row)
    # print(parsed)
    return parsed

def parse_line(line):
    parsed_line = line[0], int(line[1:])
    return parsed_line

def rotate(waypoint_coord, turn_direction, turn_amt):
    (waypoint_ew, waypoint_ns) = waypoint_coord
    turn_amt = turn_amt % 360
    if turn_direction == 'L':
        # Turn L to R
        if turn_amt == 90:
            turn_amt = 270
        elif turn_amt == 270:
            turn_amt = 90
        else:
            assert False
    if turn_amt == 0:
        (waypoint_ew, waypoint_ns) = (waypoint_ew, waypoint_ns)
    elif turn_amt == 90:
        (waypoint_ew, waypoint_ns) = (waypoint_ns, -waypoint_ew)
    elif turn_amt == 180:
        (waypoint_ew, waypoint_ns) = (-waypoint_ew, -waypoint_ns)
    elif turn_amt == 270:
        (waypoint_ew, waypoint_ns) = (-waypoint_ns, waypoint_ew)
    else:
        assert False
    return (waypoint_ew, waypoint_ns)

def part2(input):
    ew = 0
    ns = 0
    waypoint_ew = 10
    waypoint_ns = 1
    heading = 'E'
    for (action, amt) in input:
        print("State: EW, NS", ew, ns, "WP EW, NS", waypoint_ew, waypoint_ns)
        print("Action, Amt", action, amt)
        if action == 'F':
            # print("Moving F for ", heading, amt, "times")
            for i in range(amt):
                ew += waypoint_ew
                ns += waypoint_ns
        elif action == 'E':
            waypoint_ew += amt
        elif action == 'W':
            waypoint_ew -= amt
        elif action == 'N':
            waypoint_ns += amt
        elif action == 'S':
            waypoint_ns -= amt
        elif action in ['L', 'R']:
            (waypoint_ew, waypoint_ns) = rotate((waypoint_ew, waypoint_ns), action, amt)
        else:
            assert False
    print("State: EW, NS", ew, ns, "WP EW, NS", waypoint_ew, waypoint_ns)
    return abs(ew)+abs(ns)

if __name__ == "__main__":
    parsed_input = load("test.txt")
    p2 = part2(parsed_input)
    print(p2)

    parsed_input = load("input.txt")
    p2 = part2(parsed_input)
    print(p2) # 13340
