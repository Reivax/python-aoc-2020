import typing
# https://www.redblobgames.com/grids/hexagons/
# Used cube coords.

def load(filename) -> typing.List[typing.List[str]]:
    lines = []
    for line in open(filename):
        # print("Parsing line:", line)
        parsed_line = []
        split_line = [x for x in line.strip()]
        # print("Split line:", split_line)
        while len(split_line):
            # e, se, sw, w, nw, and ne
            c = split_line.pop(0)
            if c == 'e' or c == 'w':
                parsed_line.append(c)
            elif c == 's' or c == 'n':
                next_c = split_line.pop(0)
                parsed_line.append(''.join([c, next_c]))
        lines.append(parsed_line)
    return lines


def move_coord(movement, coord):
    x, y, z = coord
    if movement == 'e':
        return (x+1, y-1, z)
    elif movement == 'w':
        return (x-1, y+1, z)
    elif movement == 'nw':
        return (x, y+1, z-1)
    elif movement == 'se':
        return (x, y-1, z+1)
    elif movement == 'ne':
        return (x+1, y, z-1)
    elif movement == 'sw':
        return (x-1, y, z+1)

WHITE = True
BLACK = False

def part1(filename):
    paths: typing.List[typing.List[str]] = load(filename)
    state = {}
    for path in paths:
        coord = (0,0,0)
        print(path)
        for path_step in path:
            coord = move_coord(path_step, coord)
        state[coord] = not state.get(coord, WHITE)

    ctr = 0
    for k, v in state.items():
        if v == BLACK:
            ctr += 1
    print(ctr)
    return ctr


if __name__ == "__main__":
    assert part1("test.txt") == 10
    assert part1("input.txt") == 382
    # part1("input.txt")
