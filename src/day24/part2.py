import typing
from collections import defaultdict

def load(filename) -> typing.List[typing.List[str]]:
    lines = []
    for line in open(filename):
        # print("Parsing line:", line)
        parsed_line = []
        split_line = [x for x in line.strip()]
        # print("Split line:", split_line)
        while len(split_line):
            # e, se, sw, w, nw, and ne
            c = split_line.pop(0)
            if c == 'e' or c == 'w':
                parsed_line.append(c)
            elif c == 's' or c == 'n':
                next_c = split_line.pop(0)
                parsed_line.append(''.join([c, next_c]))
        lines.append(parsed_line)
    return lines


def move_coord(movement, coord):
    x, y, z = coord
    if movement == 'e':
        return (x+1, y-1, z)
    elif movement == 'w':
        return (x-1, y+1, z)
    elif movement == 'nw':
        return (x, y+1, z-1)
    elif movement == 'se':
        return (x, y-1, z+1)
    elif movement == 'ne':
        return (x+1, y, z-1)
    elif movement == 'sw':
        return (x-1, y, z+1)

WHITE = True
BLACK = False

def minimize_dict(state_dict):
    for k, v in [*state_dict.items()]:
        if v == WHITE:
            del state_dict[k]
    return state_dict

def tag_neighbors(neighbor_count_dict, coord):
    x,y,z = coord
    for delta in [(1,-1,0), (-1,1,0), (1,0,-1), (-1,0,1), (0,1,-1),(0,-1,1)]:
        dx, dy, dz = delta
        new_coord = (x+dx, y+dy, z+dz)
        # print("Coord", coord, "delta:", delta, "new coord", new_coord)
        neighbor_count_dict[new_coord] = neighbor_count_dict.get(new_coord, 0) + 1
    return neighbor_count_dict

def state_to_str(inst):
    if inst == WHITE:
        return "W"
    return "B:"

def advance_state(old_state_dict):
    neighbor_count_dict = {}
    new_state_dict = {}
    for coord, state in old_state_dict.items():
        if state == BLACK:
            neighbor_count_dict = tag_neighbors(neighbor_count_dict, coord)

    for coord, neighbor_count in neighbor_count_dict.items():
        current_state = old_state_dict.get(coord, WHITE)
        # print("Tile", coord, "State", state_to_str(current_state), "count", neighbor_count)
        # Keep track of only the BLACK tiles, this will keep the tile sizes smaller.
        if current_state == BLACK and (neighbor_count == 0 or neighbor_count > 2):
            # new_state_dict[coord] = WHITE
            pass
        elif current_state == WHITE and neighbor_count == 2:
            new_state_dict[coord] = BLACK
        elif current_state == BLACK:
            new_state_dict[coord] = BLACK
    return new_state_dict

def initialize_state(paths):
    state_dict = {}
    for path in paths:
        coord = (0,0,0)
        for path_step in path:
            coord = move_coord(path_step, coord)
        state_dict[coord] = not state_dict.get(coord, WHITE)
    return state_dict

def part2(filename, turns):
    paths: typing.List[typing.List[str]] = load(filename)
    state_dict = initialize_state(paths)

    state_dict = minimize_dict(state_dict)
    for turn in range(1,turns+1):
        print("Taking turn", turn)
        state_dict = advance_state(state_dict)

    ctr = 0
    for k, v in state_dict.items():
        if v == BLACK:
            ctr += 1
    print(ctr)
    return ctr

if __name__ == "__main__":
    assert part2("test.txt", 0) == 10
    assert part2("test.txt", 1) == 15
    assert part2("test.txt", 2) == 12
    assert part2("test.txt", 3) == 25
    assert part2("test.txt", 100) == 2208
    assert part2("input.txt", 100) == 3964
