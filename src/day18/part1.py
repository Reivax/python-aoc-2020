# NExt time look to https://en.wikipedia.org/wiki/Shunting-yard_algorithm

def load(input_file):
    lines = []
    with open(input_file) as f:
        for line in f:
            line = line.strip()
    return lines

def process_line(line):
    current_offset = 0
    value, offset = process_group(line, current_offset)
    return value

def evaluate(operator, l, r):
    if operator == '*':
        return l*r
    if operator == '/':
        return l//r
    if operator == '+':
        return l+r
    if operator == '-':
        return l-r

def process_group(line, current_offset):
    """

    :param line:
    :param current_offset:
    :return: value, offset
    """
    line = line.strip()
    cache = None
    operator = None
    while True:
        if current_offset >= len(line):
            break
        c = line[current_offset]
        print("C", c)
        if c == ' ':
            current_offset += 1
            continue
        elif c == '(':
            value, current_offset = process_group(line, current_offset+1)
            if cache is not None:
                cache = evaluate(operator, cache, value)
            else:
                cache = value
        elif c == ')':
            return cache, current_offset + 1
        elif c in ['*', '/', '+', '-']:
            operator = c
            current_offset += 1
        else:
            print("Its a character")
            read_value = int(c)

            if cache is not None:
                cache = evaluate(operator, cache, read_value)
            else:
                cache = read_value
            current_offset += 1

    return cache, current_offset

def part1(input_file):
    ret = 0
    with open(input_file) as inf:
        for line in inf:
            ret += process_line(line)
    return ret

if __name__ == "__main__":
    p1 = process_line("1 + 2 * 3 + 4 * 5 + 6")
    print(p1)
    assert p1 == 71
    p1 = process_line("1 + (2 * 3) + (4 * (5 + 6))")
    print(p1)
    assert p1 == 51
    p1 = process_line("2 * 3 + (4 * 5)")
    print(p1)
    assert p1 == 26

    p1 = part1("input.txt")
    print(p1)