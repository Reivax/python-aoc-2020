import typing
import json

def load(input_file):
    lines = []
    with open(input_file) as f:
        for line in f:
            line = line.strip()
    return lines

def process_line(line):
    current_offset = 0
    value, _ = process_group(line, current_offset)
    return value

def evaluate_single_operation(operator, l, r):
    # print("Evaluating single operation", operator, l, r)
    if operator == '*':
        return l*r
    elif operator == '+':
        return l+r
    else:
        assert False

def split_out_single_operation(terms, operator_indx):
    """
    replace
    [1, '+', 2, '*', 4]
    with
    [3 '*', 4]
    :param terms:
    :param operator_indx:
    :return:
    """
    cache = evaluate_single_operation(
        terms[operator_indx],
        terms[operator_indx - 1],
        terms[operator_indx + 1],
    )
    operator_indx -= 1
    # print("Removing ", terms[operator_indx])
    del terms[operator_indx]
    # print("Removing ", terms[operator_indx])
    del terms[operator_indx]
    # print("Removing and replacing ", terms[operator_indx], cache)
    terms[operator_indx] = cache
    return terms

def evaluate(terms):
    """
    [1, '+', 2, '*', 4]
    :param terms:
    :return:
    """
    while True:
        # print("State of terms:", terms)
        if not len(terms)%2:
            assert False
        if len(terms) == 1:
            return terms[0]
        if '+' in terms:
            operator_indx = terms.index('+')
            terms = split_out_single_operation(terms, operator_indx)
            continue
        elif '*' in terms:
            operator_indx = terms.index('*')
            terms = split_out_single_operation(terms, operator_indx)
            continue
        else:
            assert False

def process_group(line, current_offset):
    """

    :param line:
    :param current_offset:
    :return: value, offset
    """
    line = line.strip()
    terms = []
    while True:
        if current_offset >= len(line):
            break
        c = line[current_offset]
        if c == ' ':
            current_offset += 1
            continue
        elif c == '(':
            value, current_offset = process_group(line, current_offset+1)
            terms.append(value)
        elif c == ')':
            return evaluate(terms), current_offset + 1
        elif c in ['*', '/', '+', '-']:
            operator = c
            current_offset += 1
            terms.append(operator)
        else:
            read_value = int(c)
            terms.append(read_value)
            current_offset += 1

    return evaluate(terms), -1

def part2(input_file):
    ret = 0
    with open(input_file) as inf:
        for line in inf:
            ret += process_line(line)
    return ret

if __name__ == "__main__":
    p2 = process_line("1 + 2 * 3 + 4 * 5 + 6")
    print(p2)
    assert p2 == 231
    p2 = process_line("1 + (2 * 3) + (4 * (5 + 6))")
    print(p2)
    assert p2 == 51
    p2 = process_line("2 * 3 + (4 * 5)")
    print(p2)
    assert p2 == 46

    p2 = part2("input.txt")
    print(p2)
