import typing
import json

def load(input_file):
    rules = {}
    my_ticket:typing.List[int] = []
    nearby_tickets: typing.List[typing.List[int]] = []
    section = 0
    with open(input_file) as f:
        for line in f:
            line = line.strip()
            if len(line) == 0:
                section += 1
                continue
            elif line[-1] == ':':
                continue
            elif section==0:
                rule_name, rule_entries = parse_rule_line(line)
                rules[rule_name] = rule_entries
            elif section == 1:
                my_ticket = parse_ticket_line(line)
            elif section == 2:
                nearby_tickets.append(parse_ticket_line(line))
    return rules, my_ticket, nearby_tickets

def parse_rule_line(line):
    rule_name, rule = [x.strip() for x in line.split(":")]
    rule_left, rule_right = [x.strip() for x in rule.split(" or ")]
    rule_left_low, rule_left_high = [int(x.strip()) for x in rule_left.split("-")]
    rule_righ_low, rule_right_high = [int(x.strip()) for x in rule_right.split("-")]

    return rule_name, [(rule_left_low, rule_left_high), (rule_righ_low, rule_right_high)]


def parse_ticket_line(line):
    return [int(x.strip()) for x in line.strip().split(',')]

def find_abberant_values(ticket, rules):
    ret = []
    for ticket_value in ticket:
        for k, v in rules.items():
            low_rule, high_rule = v[0], v[1]
            if  low_rule[0] <= ticket_value <= low_rule[1] or \
                high_rule[0] <= ticket_value <= high_rule[1]:
                print("Ticket has value", ticket_value, "  It fits rule", k)
                break
        else:
            print("Ticket has value", ticket_value, "  It fits no rule")
            ret.append(ticket_value)
    return ret


def part1(input_file):
    rules, my_ticket, nearby_tickets = load(input_file)
    print("Rules", json.dumps(rules))
    print("My Ticket", my_ticket)
    aberrant_values = []
    for nearby_ticket in nearby_tickets:
        print("Nearby", nearby_ticket)
        aberrant_values.extend(find_abberant_values(nearby_ticket, rules))
    return sum(aberrant_values)

if __name__ == "__main__":
    p1 = part1("test.txt")
    print(p1)
    p1 = part1("input.txt")
    print(p1)
