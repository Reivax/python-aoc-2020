import typing
import json

def load(input_file):
    rules = {}
    my_ticket:typing.List[int] = []
    nearby_tickets: typing.List[typing.List[int]] = []
    section = 0
    with open(input_file) as f:
        for line in f:
            line = line.strip()
            if len(line) == 0:
                section += 1
                continue
            elif line[-1] == ':':
                continue
            elif section==0:
                rule_name, rule_entries = parse_rule_line(line)
                rules[rule_name] = rule_entries
            elif section == 1:
                my_ticket = parse_ticket_line(line)
            elif section == 2:
                nearby_tickets.append(parse_ticket_line(line))
    return rules, my_ticket, nearby_tickets

def parse_rule_line(line):
    rule_name, rule = [x.strip() for x in line.split(":")]
    split_rule_ranges = [x.strip() for x in rule.split(" or ")]
    rule_list = []
    for rule_range in split_rule_ranges:
        rule_low, rule_high = [int(x.strip()) for x in rule_range.split("-")]
        rule_list.append((rule_low, rule_high))
    return rule_name, rule_list


def parse_ticket_line(line):
    return [int(x.strip()) for x in line.strip().split(',')]

def find_abberant_values(ticket, rules):
    ret = []
    for ticket_value in ticket:
        for k, v in rules.items():
            low_rule, high_rule = v[0], v[1]
            if  low_rule[0] <= ticket_value <= low_rule[1] or \
                high_rule[0] <= ticket_value <= high_rule[1]:
                # print("Ticket has value", ticket_value, "  It fits rule", k)
                break
        else:
            # print("Ticket has value", ticket_value, "  It fits no rule")
            ret.append(ticket_value)
    return ret


def find_rule_column(v, tickets):
    acceptable_column = [True] * len(tickets[0])
    low_rule, high_rule = v[0], v[1]
    for current_column in range(len(acceptable_column)):
        for ticket in tickets:
            ticket_value = ticket[current_column]
            if  low_rule[0] <= ticket_value <= low_rule[1] or \
                high_rule[0] <= ticket_value <= high_rule[1]:
                continue
            else:
                acceptable_column[current_column] = False
                break

    ret = []
    for idx, valid_column in enumerate(acceptable_column):
        if valid_column:
            ret.append(idx)
    return ret

def find_singular_rule(rules):
    for rule_name, valid_columns in rules.items():
        if len(valid_columns) == 1:
            print("Identified rule", rule_name, "has only one column.")
            return rule_name

def part2(input_file):
    rules, my_ticket, nearby_tickets = load(input_file)
    print("Rules", json.dumps(rules))
    print("My Ticket", my_ticket)

    # Remove all the invalid tickets
    acceptable_nearby_tickets = [my_ticket]
    for nearby_ticket in nearby_tickets:
        # print("Nearby", nearby_ticket)
        aberrant_values = find_abberant_values(nearby_ticket, rules)
        if len(aberrant_values) == 0:
            acceptable_nearby_tickets.append(nearby_ticket)
    print(acceptable_nearby_tickets)

    # Identify which columns _might_ be the various rules.
    valid_rule_columns = {}
    for rule_name, v in rules.items():
        column_numbers = find_rule_column(v, acceptable_nearby_tickets)
        print("Rule", rule_name, "must be colum", column_numbers)
        valid_rule_columns[rule_name] = column_numbers
    print(valid_rule_columns)

    # Find only set of columns that could ever be it.
    identified_rules = {}
    for _ in range(len(valid_rule_columns)):
        singular_rule_name = find_singular_rule(valid_rule_columns)
        singular_rule_column = valid_rule_columns[singular_rule_name][0]
        print("Rule", singular_rule_name, "is column", singular_rule_column)
        identified_rules[singular_rule_name] = singular_rule_column
        copy_of_rule_names = [*valid_rule_columns.keys()]
        for rule_name in copy_of_rule_names:
            v = valid_rule_columns[rule_name]
            if singular_rule_column in v:
                v.remove(singular_rule_column)
                valid_rule_columns[rule_name] = v
        print(valid_rule_columns)
    print("identified_rules", identified_rules)

    ret = 1
    for rule_name, col_number in identified_rules.items():
        if rule_name.startswith("departure"):
            ret *= my_ticket[col_number]
    return ret



if __name__ == "__main__":
    # p1 = part2("test.txt") # [[7, 1, 14], [7, 3, 47]]
    # print(p1)
    p2 = part2("test2.txt")
    print(p2)

    p2 = part2("input.txt")
    print(p2)
