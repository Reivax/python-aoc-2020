with open("input.txt", "r") as f:
    lines = f.read().splitlines()

rules = {}
fields = {}
tickets = []
valid_tickets = []

def get_value(ticket, index):
    return ticket.split(",")[index]

for line in lines:
    line = line.strip()
    if not line:
        continue
    if "ticket" in line:
        continue
    if not line[0].isdigit(): # rule
        name = line.split(":")[0]
        ab = line.split(" ")[-3]
        cd = line.split(" ")[-1]
        a, b = ab.split("-")
        c, d = cd.split("-")
        a, b, c, d = map(int, (a, b, c, d))
        rules[name] = (a,b,c,d)
    else: # ticket
        tickets.append(line)

invalid_values = []

for ticket in tickets[1:]: # ignoring my own ticket
    values = ticket.split(",")
    values = map(int, values)
    ticket_is_valid = True
    for value in values:
        value_is_valid = False
        for rule in rules:
            a, b, c, d = rules[rule]
            if a <= value <= b or c <= value <= d:
                value_is_valid = True
        if not value_is_valid:
            invalid_values.append(value)
            ticket_is_valid = False
    if ticket_is_valid:
        valid_tickets.append(ticket)

nb_of_fields = len(valid_tickets[0].split(","))

for field in range(1, nb_of_fields):
    values = [ticket.split(",")[field] for ticket in valid_tickets]
    values = map(int, values)
    field_can_be = []
    for rule in rules:
        rule_works_for = 0
        a, b, c, d = rules[rule]
        for value in values:
            if a <= value <= b or c <= value <= d:
                rule_works_for += 1
        print(f"{rule} : marche pout {rule_works_for}")
        #print(f"{rule} : marche pout {rule_works_for} valeurs du champ {field}")
        if rule_works_for == len(valid_tickets):
            field_can_be.append(rule)
