def load(input_file):
    with open(input_file) as f:
        lines = f.readlines()
        earliest = int(lines[0])
        bus_list = lines[1].strip().split(',')
    bus_list = [int(x) for x in bus_list if x != 'x']
    return earliest, bus_list

def part1(earliest, buslist):
    print(earliest)
    print(buslist)
    done = False
    departure_difference = 0
    departure_bus = 0
    for possible_departure in range(earliest, earliest+2000):
        for bus in bus_list:
            actual_departure = possible_departure % bus
            if actual_departure == 0:
                print("Bus", bus, actual_departure)
                departure_difference = possible_departure - earliest
                departure_bus = bus
                done = True
        if done:
            break
    return departure_difference * departure_bus

if __name__ == "__main__":
    earliest, bus_list = load("test.txt")
    p1 = part1(earliest, bus_list)
    print(p1)

    earliest, bus_list = load("input.txt")
    p1 = part1(earliest, bus_list)
    print(p1)
