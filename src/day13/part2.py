from functools import reduce

def load(input_file):
    with open(input_file) as f:
        lines = f.readlines()
        earliest = int(lines[0])
        bus_list = lines[1].strip().split(',')
    return earliest, bus_list

def part1(earliest, buslist):
    from modint import ChineseRemainderConstructor

    print(earliest)
    print(buslist)
    ns = []
    mods = []
    for idx, bus in enumerate(buslist):
        if bus == 'x':
            # Need to keep ticking up the idx
            continue
        bus = int(bus)
        ns.append(bus)
        mods.append((0-idx) % bus)

    return ChineseRemainderConstructor(ns).rem(mods)

def part1_numpy(earliest, buslist):
    from sympy.ntheory.modular import crt
    print(earliest)
    print(buslist)
    timings = {}
    for idx, bus in enumerate(buslist):
        if bus == 'x':
            # Need to keep ticking up the idx
            continue
        timings[idx] = int(bus)

    r = crt(timings.values(), timings.keys())
    return r[1]-r[0]

if __name__ == "__main__":
    earliest, bus_list = load("test.txt")
    p1 = part1(earliest, bus_list)
    print("Answer: ", p1)
    p1 = part1_numpy(earliest, bus_list)
    print("Answer: ", p1)

    earliest, bus_list = load("input.txt")
    p1 = part1(earliest, bus_list)
    print("Answer: ", p1)
    p1 = part1_numpy(earliest, bus_list)
    print("Answer: ", p1)
