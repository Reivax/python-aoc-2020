import typing
from  math import sqrt

dragon = """                  # 
#    ##    ##    ###
 #  #  #  #  #  #   """
dragon_shape = []
for line in dragon.split('\n'):
    dragon_shape.append([x for x in line.strip('\n')])

def multiline_read(filename):
    with open(filename) as inf:
        lines = []
        for line in inf:
            line = line.strip()
            if len(line):
                lines.append(line)
            else:
                yield lines
                lines = []
        yield lines

def load(filename):
    tiles = []
    for multiline in multiline_read(filename):
        tile_data = list()
        id = int(multiline[0][len("Tile "):-1])
        for line in multiline[1:]:
            tile_data.append([x for x in line])
        tiles.append(Tile(tile_data, id))
    return tiles

def flip_vertical(tile_in):
    return [*reversed(tile_in)]

def flip_horizontal(tile_in):
    tile_out = []
    for row in tile_in:
        tile_out.append([*reversed(row)])
    return tile_out

def transpose(tile_in):
    return list(zip(*tile_in))

def rotate_right(tile_in):
    return list(list(x)[::-1] for x in zip(*tile_in))


# Function to print the matrix
def print_matrix(A):
    for row in A:
        print(row)

def convert_edge_to_number(tile):
    top = ''.join(tile[0])
    bot = ''.join(tile[-1])
    left = ''.join(row[0] for row in tile)
    right = ''.join(row[-1] for row in tile)
    # print("Top:", top)
    top = top.replace('.', '0').replace('#', '1')
    bot = bot.replace('.', '0').replace('#', '1')
    left = left.replace('.', '0').replace('#', '1')
    right = right.replace('.', '0').replace('#', '1')
    # print("Top:", top)

    top_int = max(int(top, 2), int(top[::-1], 2))
    bot_int = max(int(bot, 2), int(bot[::-1], 2))
    left_int = max(int(left, 2), int(left[::-1], 2))
    right_int = max(int(right, 2), int(right[::-1], 2))
    # print("Top:", top_int)
    return top_int, bot_int, left_int, right_int

def remove_bnrders(tile_data):
    ret = []
    for row in tile_data[1:-1]:
        ret.append(row[1:-1])
    return ret

class Tile(object):
    def __init__(self, tile_data, tile_id):
        (t, b, l, r) = convert_edge_to_number(tile_data)
        self.t = t
        self.b = b
        self.l = l
        self.r = r
        self.tile_id = tile_id

        self.tile_data = tile_data

    def rotate_right(self):
        tmp = self.t
        self.t = self.l
        self.l = self.b
        self.b = self.r
        self.r = tmp
        self.tile_data = rotate_right(self.tile_data)

    def flip_vertical(self):
        tmp = self.t
        self.t = self.b
        self.b = tmp
        self.tile_data = flip_vertical(self.tile_data)

    def flip_horizontal(self):
        tmp = self.r
        self.r = self.l
        self.l = tmp
        self.tile_data = flip_horizontal(self.tile_data)

    def __contains__(self, item):
        return item in [self.t, self.b, self.l, self.r]

    def __repr__(self):
        return "Tile " + str(self.tile_id)

def check_match_below(top_tile, bottom_tile):
    for ct, cb in zip(top_tile[-1],bottom_tile[0]):
        if ct != cb:
            return False
    return True

def check_match_between(left_tile, right_tile):
    for cl, cr in zip(
            (row[-1] for row in left_tile),
            (row[0] for row in right_tile)
    ):
        if cl != cr:
            return False
    return True


def find_edges(tiles: typing.List[Tile]):
    edge_counts = {}
    for tile in tiles:
        (t, b, l, r) = tile.t, tile.b, tile.l, tile.r
        for x in [t, b, l, r]:
            entry = edge_counts.get(x, list())
            entry.append(tile)
            edge_counts[x] = entry
    # These are the edges and corners
    uncoupled_sides = set()
    for k, v in edge_counts.items():
        if len(v) == 1:
            uncoupled_sides.add(v[0])
    return uncoupled_sides

def find_tiles_with_two_neighbors(tiles: typing.List[Tile]):
    edge_counts = {}
    for tile in tiles:
        (t, b, l, r) = tile.t, tile.b, tile.l, tile.r
        for x in [t, b, l, r]:
            entry = edge_counts.get(x, list())
            entry.append(tile.tile_id)
            edge_counts[x] = entry
    # These are the edges and corners
    uncoupled_sides = set()
    # These are the corners.
    double_uncoupled_sides = set()
    for k, v in edge_counts.items():
        if len(v) == 1:
            tile_id = v[0]
            if tile_id in uncoupled_sides:
                double_uncoupled_sides.add(tile_id)
            else:
                uncoupled_sides.add(v[0])
    print(double_uncoupled_sides)
    return double_uncoupled_sides

def make_top_left(tiles: typing.List[Tile]):
    # K, v of edge_id, Tile object
    edge_counts = {}
    for tile in tiles:
        (t, b, l, r) = tile.t, tile.b, tile.l, tile.r
        for x in [t, b, l, r]:
            entry = edge_counts.get(x, list())
            entry.append(tile)
            edge_counts[x] = entry
    # These are the edges and corners
    uncoupled_sides:typing.List[int] = list()
    exposed_edges = set()
    for k, v in edge_counts.items():
        k:str = k
        v:typing.List[Tile] = v
        if len(v) == 1:
            tile = v[0]
            uncoupled_sides.append(tile)
            exposed_edges.add(k)
    seen:typing.Set[Tile] = set()
    corners:typing.List[Tile] = list()
    for tile in uncoupled_sides:
        if tile in seen:
            corners.append(tile)
        seen.add(tile)

    print(corners)

    corner_edges = list()
    for exposed_edge in exposed_edges:
        for tile in corners:
            if exposed_edge in tile:
                corner_edges.append(exposed_edge)
                print("Tile {} has exposed edge {}".format(tile.tile_id, exposed_edge))

    top_left_tile:Tile = corners[0]
    # Okay, fix the orientation of this tile.
    print("Identified top-left, moving into correct position")

    top_left_tile.rotate_right()
    print()
    print_matrix(top_left_tile.tile_data)

    top_left_tile.rotate_right()
    print()
    print_matrix(top_left_tile.tile_data)

    top_left_tile.flip_horizontal()
    print()
    print_matrix(top_left_tile.tile_data)

    # Ok, located top-left tile, and oriented it.
    return top_left_tile

def make_top_row(tiles, all_reamining_edge_tiles, top_left_tile):
    row = []
    row.append(top_left_tile)
    row_max_size = sqrt(len(tiles))
    print("All remining edge tiles:")
    print(all_reamining_edge_tiles)
    while len(row) < row_max_size:
        all_reamining_edge_tiles -= set(row)
        current_correct_tile = row[-1]
        for tile_candidate in all_reamining_edge_tiles:
            tile_candidate:Tile = tile_candidate
            if current_correct_tile.r in tile_candidate:
                print("Located neighbor:  Current tile {} adjoins with {}".format(current_correct_tile, tile_candidate))
                all_reamining_edge_tiles.remove(tile_candidate)
                while tile_candidate.l != current_correct_tile.r:
                    tile_candidate.rotate_right()
                if not check_match_between(current_correct_tile.tile_data, tile_candidate.tile_data):
                    tile_candidate.flip_vertical()
                row.append(tile_candidate)
                break
        else:
            print("Cannot do it")
            break
    print("Row", row)
    return row


def make_row(all_remaining_tiles, row_above, row_max_size):
    row = []
    tile_above = row_above[0]
    # Get the anchoring tile along the left side.
    for tile_candidate in all_remaining_tiles:
        tile_candidate:Tile = tile_candidate
        if tile_above.b in tile_candidate:
            print("Located Below:  tile_above {} adjoins with {}".format(tile_above, tile_candidate))
            all_remaining_tiles.remove(tile_candidate)
            while tile_candidate.t != tile_above.b:
                tile_candidate.rotate_right()
            if not check_match_below(tile_above.tile_data, tile_candidate.tile_data):
                print("This tile needs a LR flip", tile_candidate)
                tile_candidate.flip_horizontal()
                print_matrix(tile_above.tile_data)
                print()
                print_matrix(tile_candidate.tile_data)
            row.append(tile_candidate)
            break
    else:
        print("No match, could not start row")
    # print_matrix(tile_candidate.tile_data)

    while len(row) < row_max_size:
        all_remaining_tiles -= set(row)
        current_correct_tile = row[-1]
        print("All remaining tiles: ", all_remaining_tiles)
        print("Looking for match of ", current_correct_tile)
        print("Current correct r:", current_correct_tile.r)
        for tile_candidate in all_remaining_tiles:
            tile_candidate:Tile = tile_candidate
            print("Tile candidate", tile_candidate)
            print("T, B, L, R", tile_candidate.t, tile_candidate.b, tile_candidate.r, tile_candidate.l, tile_candidate.r)
            if current_correct_tile.r in tile_candidate:
                print("Located neighbor:  Current tile {} adjoins with {}".format(current_correct_tile, tile_candidate))
                all_remaining_tiles.remove(tile_candidate)
                # Rotate it to match the left-right correctly.
                while tile_candidate.l != current_correct_tile.r:
                    tile_candidate.rotate_right()
                # Flip it vertically to actually match.
                if not check_match_between(current_correct_tile.tile_data, tile_candidate.tile_data):
                    tile_candidate.flip_vertical()
                row.append(tile_candidate)
                break
        else:
            print("No match.", current_correct_tile)
            print("Looking to match t, b, l, >r<", current_correct_tile.t, current_correct_tile.b, current_correct_tile.l, current_correct_tile.r)
            for tile in all_remaining_tiles:
                print(tile, tile.t, tile.b, tile.l, tile.r)
            # assert False
            break

    return row


def merge_the_world(game_board, visible_debug=False):
    ret = []
    for game_board_row in game_board:
        for idx in range(0, 8):
            output_row = []
            for game_piece in game_board_row:
                output_row.extend(remove_bnrders(game_piece.tile_data)[idx])
                if visible_debug:
                    output_row.append("  ")
            ret.append(output_row)
        if visible_debug:
            ret.append([])
    return ret

def check_for_dragon(world_window, dragon_shape):
    print("Looking for dragon here:")
    print_matrix(dragon_shape)
    if len(world_window) < len(dragon_shape) \
            or len(world_window[2]) < len(dragon_shape[0]):
        print("skipping this one.")
        return False
    print_matrix(world_window)
    for world_row, dragon_row in zip(world_window, dragon_shape):
        for world_cell, dragon_cell in zip(world_row, dragon_row):
            if dragon_cell == '#' and world_cell != '#':
                return False
    print("This was a hit.")
    return True

def remove_dragons(world):
    # print("Count", count_hashes(dragon_shape)) # 15
    print_matrix(dragon_shape)
    dragon_x_size = len(dragon_shape[0])
    dragon_y_size = len(dragon_shape)
    world_x_size = len(world[0])
    world_y_size = len(world)

    print("dragon_x_size", dragon_x_size)
    print("dragon_y_size", dragon_y_size)
    print("world_x_size", world_x_size)
    print("world_y_size", world_y_size)

    print("Looking for dragons")

    dragon_count = 0
    for y in range(0, world_y_size - dragon_y_size):
        for x in range(0, world_x_size - dragon_x_size):
            print("Looking at yx", y, x)
            subworld = []
            subworld.append(world[y+0][x:x+dragon_x_size])
            subworld.append(world[y+1][x:x+dragon_x_size])
            subworld.append(world[y+2][x:x+dragon_x_size])
            if check_for_dragon(subworld, dragon_shape):
                dragon_count += 1
    print("Found dragons:", dragon_count)
    return dragon_count

def count_the_dragons(big_merged_world):
    for limit in range(0, 4):
        dragon_count = remove_dragons(big_merged_world)
        if dragon_count > 0:
            print("We hit a dragon!  First loop", dragon_count, limit)
            return dragon_count
        else:
            big_merged_world = rotate_right(big_merged_world)
    big_merged_world = flip_horizontal(big_merged_world)
    for limit in range(0, 4):
        dragon_count = remove_dragons(big_merged_world)
        if dragon_count > 0:
            print("We hit a dragon!  Second loop", dragon_count, limit)
            return dragon_count
        else:
            big_merged_world = rotate_right(big_merged_world)
    print("Never found anything, exiting")
    return 0

def part2(filename):
    tiles = load(filename)
    top_left_tile = make_top_left(tiles)
    # print_matrix(top_left_tile.tile_data)

    all_reamining_edge_tiles = set(find_edges(tiles))
    max_game_rows = sqrt(len(tiles))
    # The game board is sqrt(len(tiles)) to a side.  Test is 3x3, input is 12x12
    game_board = []
    game_board.append(
        make_top_row(tiles, all_reamining_edge_tiles, top_left_tile)
    )
    all_remaining_tiles = set(tiles.copy())
    all_remaining_tiles -= set(game_board[-1])
    while len(game_board) < max_game_rows:
        game_board.append(
            make_row(all_remaining_tiles, game_board[-1], max_game_rows)
        )
    print("reamining", len(all_remaining_tiles), all_remaining_tiles)
    print_matrix(game_board)
    for game_board_row in game_board:
        if len(game_board_row) != max_game_rows:
            assert False

    big_merged_world = merge_the_world(game_board, visible_debug=False)
    # print_matrix(big_merged_world)
    # Alright, lets go dragon hunting.

    dragon_count = count_the_dragons(big_merged_world)

    hash_count = count_hashes(big_merged_world) - dragon_count * 15
    print("Hash count of the dragon", count_hashes(dragon_shape))
    print("Hash count", hash_count)

def count_hashes(matrix):
    ctr = 0
    for row in matrix:
        for cell in row:
            if cell == '#':
                ctr += 1
    return ctr


if __name__ == "__main__":
    """
    ||My first thought it to convert every edge to a number, by replacing the `.` and `#` with `0`
    and `1`.||  ||Then convert that binary.  Then, reverse the string, and convert_taht_ to binary,
    and keep only the max||
    That reduces the problem say to an id and four numbers.  Now just match the numbers.
    """
    # part2("test.txt")
    part2("input.txt")
    # 2119 is too low.  Found 15 dragons.
    # 2194 is too low.  Found 5 dragons.
    # 2269 is the max, but it didn't say if we were high or low.
