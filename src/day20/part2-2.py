import typing
from math import sqrt

# Fully working, if overly verbose, solution.
# Also takes a lucky guess at the opening tile orientation.

dragon = """                  # 
#    ##    ##    ###
 #  #  #  #  #  #   """
dragon_shape = []
for line in dragon.split('\n'):
    dragon_shape.append([x for x in line.strip('\n')])

def multiline_read(filename):
    with open(filename) as inf:
        lines = []
        for line in inf:
            line = line.strip()
            if len(line):
                lines.append(line)
            else:
                yield lines
                lines = []
        yield lines

def load(filename):
    tiles = []
    for multiline in multiline_read(filename):
        tile_data = list()
        id = int(multiline[0][len("Tile "):-1])
        for line in multiline[1:]:
            tile_data.append([x for x in line])
        tile = Tile(tile_data, id)
        tiles.append(tile)
        print("Initilated tile:", tile)
    return tiles

def matrix_flip_vertical(tile_in):
    return tile_in[::-1]

def matrix_flip_horizontal(tile_in):
    tile_out = []
    for row in tile_in:
        tile_out.append(row[::-1])
    return tile_out

def matrix_rotate_right(tile_in):
    return list(list(x)[::-1] for x in zip(*tile_in))


# Function to print the matrix
def print_matrix(tile_in):
    for row in tile_in:
        print(row)

def convert_edge_to_number(tile):
    top = ''.join(tile[0])
    bot = ''.join(tile[-1])
    left = ''.join(row[0] for row in tile)
    right = ''.join(row[-1] for row in tile)
    ret = []
    for line in [top, bot, left, right]:
        line = line.replace('.', '0').replace('#', '1')
        tile_number = max(int(line, 2), int(line[::-1], 2))
        ret.append(tile_number)
        print("Converted edge {} to number {}", line, tile_number)
    return ret

class Tile(object):
    def __init__(self, tile_data, tile_id):
        (t, b, l, r) = convert_edge_to_number(tile_data)
        self.t = t
        self.b = b
        self.l = l
        self.r = r
        self.tile_id = tile_id

        self.tile_data = tile_data

    def matrix_rotate_right(self):
        tmp = self.t
        self.t = self.l
        self.l = self.b
        self.b = self.r
        self.r = tmp
        self.tile_data = matrix_rotate_right(self.tile_data)

    def matrix_flip_vertical(self):
        tmp = self.t
        self.t = self.b
        self.b = tmp
        self.tile_data = matrix_flip_vertical(self.tile_data)

    def matrix_flip_horizontal(self):
        tmp = self.r
        self.r = self.l
        self.l = tmp
        self.tile_data = matrix_flip_horizontal(self.tile_data)

    def remove_borders(self):
        ret = []
        for row in self.tile_data[1:-1]:
            ret.append(row[1:-1])
        return ret

    def __contains__(self, item):
        return item in [self.t, self.b, self.l, self.r]

    def __repr__(self):
        return "Tile " + str(self.tile_id)

def check_match_below(top_tile, bottom_tile):
    for ct, cb in zip(top_tile[-1],bottom_tile[0]):
        if ct != cb:
            return False
    return True

def check_match_between(left_tile, right_tile):
    for cl, cr in zip(
            (row[-1] for row in left_tile),
            (row[0] for row in right_tile)
    ):
        if cl != cr:
            return False
    return True

def make_top_left(tiles: typing.List[Tile]):
    # K, v of edge_id, Tile object
    edge_counts = {}
    for tile in tiles:
        for x in [tile.t, tile.b, tile.l, tile.r]:
            entry = edge_counts.get(x, list())
            entry.append(tile)
            edge_counts[x] = entry
    # These are the edges and corners
    uncoupled_sides:typing.List[Tile] = list()
    exposed_edges = set()
    for k, v in edge_counts.items():
        k:str = k
        v:typing.List[Tile] = v
        if len(v) == 1:
            tile = v[0]
            uncoupled_sides.append(tile)
            exposed_edges.add(k)
    seen:typing.Set[Tile] = set()
    corners:typing.List[Tile] = list()
    for tile in uncoupled_sides:
        if tile in seen:
            corners.append(tile)
        seen.add(tile)

    # Pick a corner piece, and make that the top left.
    top_left_tile:Tile = corners[0]
    # Okay, fix the orientation of this tile.
    print("Identified top-left, moving into correct orientation, ", top_left_tile)
    # Rotate right until the left side is an exposed edge.
    while top_left_tile.l not in exposed_edges:
        pritn("Rotating corner piece to start..")
        top_left_tile.matrix_rotate_right()
    # If the top isn't and exposed edge, give it one more rotation.
    if top_left_tile.t not in exposed_edges:
        print("One more rotation.")
        top_left_tile.matrix_rotate_right()
    print("Identified top-left, adequately rotated, ", top_left_tile)
    return top_left_tile

def make_top_row(remaining_tiles, top_left_tile, row_max_size):
    row = []
    row.append(top_left_tile)
    print("All remaining_tiles tiles:")
    print(remaining_tiles)
    while len(row) < row_max_size:
        current_correct_tile = row[-1]
        for tile_candidate in remaining_tiles:
            tile_candidate:Tile = tile_candidate
            if current_correct_tile.r in tile_candidate:
                print("Located neighbor:  Current tile {} adjoins with {}".format(current_correct_tile, tile_candidate))
                # Identified that the currently selected tile has one side that matches the currently placed tile.
                remaining_tiles.remove(tile_candidate)
                # rotate the tile until the matching edges are on the same side.
                while tile_candidate.l != current_correct_tile.r:
                    tile_candidate.matrix_rotate_right()
                # If necessary, flip tile over to make the edges align.
                if not check_match_between(current_correct_tile.tile_data, tile_candidate.tile_data):
                    tile_candidate.matrix_flip_vertical()
                # Add tile to the row.
                row.append(tile_candidate)
                break
        else:
            print("Cannot do it")
            assert False
    print("Row", row)
    return row


def make_row(all_remaining_tiles, row_above, row_max_size):
    row = []
    tile_above = row_above[0]
    # Get the anchoring tile along the left side.
    for tile_candidate in all_remaining_tiles:
        tile_candidate:Tile = tile_candidate
        if tile_above.b in tile_candidate:
            print("Located Below:  tile_above {} adjoins with {}".format(tile_above, tile_candidate))
            all_remaining_tiles.remove(tile_candidate)
            while tile_candidate.t != tile_above.b:
                tile_candidate.matrix_rotate_right()
            if not check_match_below(tile_above.tile_data, tile_candidate.tile_data):
                tile_candidate.matrix_flip_horizontal()
            row.append(tile_candidate)
            break
    else:
        print("No match, could not start row")
        assert False
    # print_matrix(tile_candidate.tile_data)

    # Fill out the rest of the row.
    while len(row) < row_max_size:
        all_remaining_tiles -= set(row)
        current_correct_tile = row[-1]
        tile_above = row_above[len(row)]
        # print("All remaining tiles: ", all_remaining_tiles)
        # print("Looking for match of ", current_correct_tile)
        # print("Current correct r:", current_correct_tile.r)
        for tile_candidate in all_remaining_tiles:
            tile_candidate:Tile = tile_candidate
            # print("Tile candidate", tile_candidate)
            # print("T, B, L, R", tile_candidate.t, tile_candidate.b, tile_candidate.r, tile_candidate.l, tile_candidate.r)
            if current_correct_tile.r in tile_candidate:
                print("Located neighbor:  Current tile {} adjoins with {}".format(current_correct_tile, tile_candidate))
                all_remaining_tiles.remove(tile_candidate)
                # Rotate it to match the left-right correctly.
                while tile_candidate.l != current_correct_tile.r:
                    tile_candidate.matrix_rotate_right()
                # Flip it vertically to actually match.
                if not check_match_between(current_correct_tile.tile_data, tile_candidate.tile_data):
                    tile_candidate.matrix_flip_vertical()
                # The top edge of this tile should also match the bottom edge of the above tile by this point.
                assert check_match_below(tile_above.tile_data, tile_candidate.tile_data)
                row.append(tile_candidate)
                break
        else:
            print("No match.", current_correct_tile)
            print("Looking to match t, b, l, >r<", current_correct_tile.t, current_correct_tile.b, current_correct_tile.l, current_correct_tile.r)
            for tile in all_remaining_tiles:
                print(tile, tile.t, tile.b, tile.l, tile.r)
            assert False

    return row


def merge_the_world(game_board, visible_debug=False):
    ret = []
    for game_board_row in game_board:
        for idx in range(0, 8):
            output_row = []
            for game_piece in game_board_row:
                output_row.extend(game_piece.remove_borders()[idx])
                if visible_debug:
                    output_row.append("  ")
            ret.append(output_row)
        if visible_debug:
            ret.append([])
    return ret

def check_for_dragon(world_window, dragon_shape):
    if len(world_window) < len(dragon_shape) \
            or len(world_window[2]) < len(dragon_shape[0]):
        print("skipping this one.")
        return False
    # print_matrix(dragon_shape)
    # print_matrix(world_window)
    for world_row, dragon_row in zip(world_window, dragon_shape):
        for world_cell, dragon_cell in zip(world_row, dragon_row):
            if dragon_cell == '#' and world_cell != '#':
                return False
    print("This was a hit.")
    return True

def attempt_count_dragons(world):
    # print("Count", count_hashes(dragon_shape)) # 15
    print_matrix(dragon_shape)
    dragon_x_size = len(dragon_shape[0])
    dragon_y_size = len(dragon_shape)
    world_x_size = len(world[0])
    world_y_size = len(world)

    print("dragon_x_size", dragon_x_size)
    print("dragon_y_size", dragon_y_size)
    print("world_x_size", world_x_size)
    print("world_y_size", world_y_size)

    print("Looking for dragons")

    dragon_count = 0
    for y in range(0, world_y_size - dragon_y_size):
        for x in range(0, world_x_size - dragon_x_size):
            # print("Dragon search at yx", y, x)
            subworld = []
            subworld.append(world[y+0][x:x+dragon_x_size])
            subworld.append(world[y+1][x:x+dragon_x_size])
            subworld.append(world[y+2][x:x+dragon_x_size])
            if check_for_dragon(subworld, dragon_shape):
                dragon_count += 1
    print("Found dragons:", dragon_count)
    return dragon_count

def count_the_dragons(big_merged_world):
    # Rotate the world until we find a map with dragons.
    for limit in range(0, 4):
        dragon_count = attempt_count_dragons(big_merged_world)
        if dragon_count > 0:
            print("We hit a dragon!  First loop", dragon_count, limit)
            return dragon_count
        else:
            big_merged_world = matrix_rotate_right(big_merged_world)
    # STill no dragons?  Flip the world, and rotate it again.
    big_merged_world = matrix_flip_horizontal(big_merged_world)
    for limit in range(0, 4):
        dragon_count = attempt_count_dragons(big_merged_world)
        if dragon_count > 0:
            print("We hit a dragon!  Second loop", dragon_count, limit)
            return dragon_count
        else:
            big_merged_world = matrix_rotate_right(big_merged_world)
    print("Never found anything, exiting")
    return 0

def part2(filename):
    tiles = load(filename)
    top_left_tile = make_top_left(tiles)
    # print_matrix(top_left_tile.tile_data)

    all_remaining_tiles = set(tiles)
    all_remaining_tiles.remove(top_left_tile)
    max_game_rows = sqrt(len(tiles))
    # The game board is sqrt(len(tiles)) to a side.  Test is 3x3, input is 12x12
    game_board = []
    game_board.append(
        make_top_row(all_remaining_tiles, top_left_tile, max_game_rows)
    )
    while len(game_board) < max_game_rows:
        game_board.append(
            make_row(all_remaining_tiles, game_board[-1], max_game_rows)
        )
    print("reamining", len(all_remaining_tiles), all_remaining_tiles)
    print_matrix(game_board)
    for game_board_row in game_board:
        if len(game_board_row) != max_game_rows:
            assert False

    big_merged_world = merge_the_world(game_board, visible_debug=False)
    print_matrix(big_merged_world)
    # Alright, lets go dragon hunting.

    dragon_count = count_the_dragons(big_merged_world)

    hash_count = count_hashes(big_merged_world) - dragon_count * 15
    print("Hash count of the dragon", count_hashes(dragon_shape))
    print("Hash count", hash_count)
    return hash_count

def count_hashes(matrix):
    ctr = 0
    for row in matrix:
        for cell in row:
            if cell == '#':
                ctr += 1
    return ctr


if __name__ == "__main__":
    assert part2("test.txt") == 273
    # assert part2("input.txt") == 2424
    # 2119 is too low.  Found 15 dragons.
    # 2194 is too low.  Found 5 dragons.
    # 2269 is the max, but it didn't say if we were high or low.
