def multiline_read(filename):
    with open(filename) as inf:
        lines = []
        for line in inf:
            line = line.strip()
            if len(line):
                lines.append(line)
            else:
                yield lines
                lines = []
        yield lines

def load(filename):
    tiles = []
    for multiline in multiline_read(filename):
        tile = list()
        id = int(multiline[0][len("Tile "):-1])
        for line in multiline[1:]:
            tile.append([x for x in line])
        tiles.append((tile, id))
    return tiles

def convert_edge_to_number(tile):
    top = ''.join(tile[0])
    bot = ''.join(tile[-1])
    left = ''.join(row[0] for row in tile)
    right = ''.join(row[-1] for row in tile)
    print("Top:", top)
    top = top.replace('.', '0').replace('#', '1')
    bot = bot.replace('.', '0').replace('#', '1')
    left = left.replace('.', '0').replace('#', '1')
    right = right.replace('.', '0').replace('#', '1')
    print("Top:", top)

    top_int = max(int(top, 2), int(top[::-1], 2))
    bot_int = max(int(bot, 2), int(bot[::-1], 2))
    left_int = max(int(left, 2), int(left[::-1], 2))
    right_int = max(int(right, 2), int(right[::-1], 2))
    print("Top:", top_int)
    return top_int, bot_int, right_int, left_int

def find_tiles_with_two_neighbors(tiles):
    edge_counts = {}
    for tile_data, tile_id in tiles:
        (t,b,l,r) = convert_edge_to_number(tile_data)
        print("Parse of {} became".format(tile_id), t, b, l, r)
        for x in [t,b,l,r]:
            entry = edge_counts.get(x, list())
            entry.append(tile_id)
            edge_counts[x] = entry
    uncoupled_sides = set()
    double_uncoupled_sides = set()
    for k, v in edge_counts.items():
        if len(v) == 1:
            tile_id = v[0]
            if tile_id in uncoupled_sides:
                double_uncoupled_sides.add(tile_id)
            else:
                uncoupled_sides.add(v[0])
    print(double_uncoupled_sides)
    print(prod(double_uncoupled_sides))

def prod(vals):
    ret = 1
    for val in vals:
        ret *= val
    return ret

def part1(filename):
    tiles = load(filename)
    print("Total number of tiles", len(tiles))
    find_tiles_with_two_neighbors(tiles)

if __name__ == "__main__":
    part1("test.txt")
