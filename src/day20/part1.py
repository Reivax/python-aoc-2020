def multiline_read(filename):
    with open(filename) as inf:
        lines = []
        for line in inf:
            line = line.strip()
            if len(line):
                lines.append(line)
            else:
                yield lines
                lines = []
        yield lines

def load(filename):
    tiles = []
    for multiline in multiline_read(filename):
        tile = list()
        id = int(multiline[0][len("Tile "):-1])
        for line in multiline[1:]:
            tile.append([x for x in line])
        tiles.append((tile, id))
    return tiles

def part1(filename):
    tiles = load(filename)
    print(tiles[0])
    print(tiles)
    printMatrix(tiles[0][0])
    print()
    printMatrix(transpose(tiles[0][0]))

def flip_vertical(tile_in):
    return [*reversed(tile_in)]

def flip_horizontal(tile_in):
    tile_out = []
    for row in tile_in:
        tile_out.append(*reversed(row))
    return tile_out

def transpose(tile_in):
    return list(zip(*tile_in))

def rotate_right(tile_in):
    return list(list(x)[::-1] for x in zip(*tile_in))

def check_match_below(top_tile, bottom_tile):
    for ct, cb in zip(
            (top_tile[-1]),
            (bottom_tile[1])
    ):
        if ct != cb:
            return False
    return True

def check_match_between(left_tile, right_tile):
    for cl, cr in zip(
            (row[-1] for row in left_tile),
            (row[0] for row in right_tile)
    ):
        if ct != cb:
            return False
    return True


# Function to print the matrix
def printMatrix(A):
    for row in A:
        print(row)
# An Inplace function to rotate
# N x N matrix by 90 degrees in
# anti-clockwise direction
def rotateMatrix(mat):
    N = 10
    # Consider all squares one by one
    for x in range(0, int(N / 2)):
        # Consider elements in group
        # of 4 in current square
        for y in range(x, N - x - 1):
            # store current cell in temp variable
            temp = mat[x][y]
            # move values from right to top
            mat[x][y] = mat[y][N - 1 - x]
            # move values from bottom to right
            mat[y][N - 1 - x] = mat[N - 1 - x][N - 1 - y]
            # move values from left to bottom
            mat[N - 1 - x][N - 1 - y] = mat[N - 1 - y][x]
            # assign temp to left
            mat[N - 1 - y][x] = temp

if __name__ == "__main__":
    part1("test.txt")
