import functools
def load(input_file):
    parsed = []
    for line in open(input_file):
        parsed.append(int(line))
    parsed.sort()
    return parsed

def part1(parsed):
    joltage_ctr = {
        1: 0,
        2: 0,
        3: 0,
    }
    remaining = parsed.copy()
    laptop_joltage = max(parsed) + 3
    current_jolts = 0
    while current_jolts <= laptop_joltage:
        if not len(remaining):
            break
        next_joltage = remaining.pop(0)
        diff = next_joltage - current_jolts
        joltage_ctr[diff] = joltage_ctr[diff] + 1
        # print(next_joltage, diff)
        current_jolts = next_joltage
    diff = laptop_joltage - current_jolts
    joltage_ctr[diff] = joltage_ctr[diff] + 1
    # print(joltage_ctr)
    print(joltage_ctr[1] * joltage_ctr[3]) # 2112
    return joltage_ctr[1] * joltage_ctr[3]

def recur_trib(n):
   if n <= 2:
       return 0
   elif n == 3:
       return 1
   else:
       return recur_trib(n-1) + recur_trib(n-2) + recur_trib(n-3)

def arrangement_counter(arrangable_set):
    ret = recur_trib(len(arrangable_set)+3)
    # print(ret, arrangable_set)
    return max(ret, 1)

def part_2(parsed_input):
    """
    Extremely spoilery general solution to part B: the tribonacci sequence, for the purposes of this
    question, is 1, 1, 2, 4, 7, 13, 24, 44, ... For this problem, you're counting runs of 1-steps.
    A run of length 0 or 1 yields 1 choice; a run of length 2 yields 2 choices; a run of length 3
    yields 4 choices, etc. Take the product of all the choices. That's it!
    """
    lines = parsed_input.copy()
    lines.sort()
    lines.append(max(lines)+3)
    accumulator = 1
    arrangable_set = []
    laptop_joltage = max(lines)
    current_jolts = 0
    while current_jolts <= laptop_joltage:
        if not len(lines):
            break
        next_joltage = lines.pop(0)
        diff = next_joltage - current_jolts
        if diff == 1:
            arrangable_set.append(next_joltage)
        else:
            accumulator *= arrangement_counter(arrangable_set)
            arrangable_set = []
        # print(next_joltage, diff)
        current_jolts = next_joltage

    accumulator *= arrangement_counter(arrangable_set)
    print(accumulator)
    return accumulator



def direct_recursion(parsed_input):
    joltage_adapters = parsed_input.copy()
    # Sorted
    joltage_adapters.sort()
    # Add the laptop itself.
    joltage_adapters.append(max(joltage_adapters)+3)
    @functools.lru_cache(maxsize=None)
    def alt_recur(current_joltage):
        if current_joltage == joltage_adapters[-1]:
            return 1
        ret = 0
        for next_joltage in joltage_adapters:
            if current_joltage < next_joltage <= current_joltage + 3:
                ret += alt_recur(next_joltage)
        return ret

    ret = alt_recur(0)
    print(ret)
    return ret

def recur_with_cache(parsed_input):
    joltage_adapters = parsed_input.copy()
    # Sorted
    joltage_adapters.sort()
    # Add the laptop itself.
    joltage_adapters.append(max(joltage_adapters) + 3)
    answer_cache = {}

    def recur(current_joltage):
        if current_joltage == joltage_adapters[-1]:
            return 1
        if current_joltage in answer_cache:
            return answer_cache[current_joltage]
        ret = 0
        for next_joltage in joltage_adapters:
            if current_joltage < next_joltage <= current_joltage + 3:
                ret += recur(next_joltage)
        answer_cache[current_joltage] = ret
        return ret
    ret = recur(0)
    print(ret)
    return ret

def part_2_dict(parsed_input):
    joltage_adapters = parsed_input.copy()
    # Sorted
    joltage_adapters.sort()
    # Add the laptop itself.
    # joltage_adapters.append(max(joltage_adapters)+3)
    paths = {}
    paths[0] = 1
    paths[1] = 1
    paths[2] = 2
    paths[3] = 4
    for jolt in joltage_adapters[3:]:
        a = paths.get(jolt - 1, 0)
        b = paths.get(jolt - 2, 0)
        c = paths.get(jolt - 3, 0)
        paths[jolt] = a+b+c

    last = joltage_adapters[-1]
    print(paths[last])
    return paths[last]


if __name__ == "__main__":
    parsed_input = load("test.txt")
    assert part1(parsed_input) == 35

    assert part_2(parsed_input) == 8
    assert part_2_dict(parsed_input) == 8
    assert direct_recursion(parsed_input) == 8
    assert recur_with_cache(parsed_input) == 8

    parsed_input = load("test2.txt")
    assert part1(parsed_input) == 220

    assert part_2(parsed_input) == 19208
    assert part_2_dict(parsed_input) == 19208
    assert direct_recursion(parsed_input) == 19208
    assert recur_with_cache(parsed_input) == 19208

    parsed_input = load("input.txt")
    assert part1(parsed_input) == 2112

    assert part_2(parsed_input) == 3022415986688
    assert part_2_dict(parsed_input) == 3022415986688
    assert direct_recursion(parsed_input) == 3022415986688
    assert recur_with_cache(parsed_input) == 3022415986688
    #for i in range(10):
    #     print(i, recur_trib(i))
