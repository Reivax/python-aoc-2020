import typing

def load(filename) -> typing.List[typing.List]:
    player = 0
    deck = [None] * 2
    deck[0] = list()
    deck[1] = list()
    for line in open(filename):
        line = line.strip()
        if line.startswith("Player"):
            continue
        if len(line) == 0:
            player += 1
            continue
        deck[player].append(int(line))
    return deck

def part1(filename):
    deck: typing.List[typing.List] = load(filename)
    print(deck)
    while len(deck[0]) > 0 and len(deck[1]) > 0:
        play_0 = deck[0].pop(0)
        play_1 = deck[1].pop(0)
        if play_1 > play_0:
            deck[1].append(play_1)
            deck[1].append(play_0)
        if play_0 > play_1:
            deck[0].append(play_0)
            deck[0].append(play_1)
        print(deck)

    winner = 1 if len(deck[1]) > len(deck[0]) else 0
    current_multiplier = len(deck[winner])
    score = 0
    while len(deck[winner]) > 0:
        card = deck[winner].pop(0)
        score += card * current_multiplier
        current_multiplier -= 1
    print(score)

    return score

if __name__ == "__main__":
    assert part1("test.txt") == 306
    # part1("input.txt")
