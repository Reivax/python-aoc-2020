import typing
import copy

def load(filename) -> typing.List[typing.List]:
    player = 0
    deck = [None] * 2
    deck[0] = list()
    deck[1] = list()
    for line in open(filename):
        line = line.strip()
        if line.startswith("Player"):
            continue
        if len(line) == 0:
            player += 1
            continue
        deck[player].append(int(line))
    return deck

def part2(filename):
    deck: typing.List[typing.List] = load(filename)

    winner = recursive_game(deck)
    print("Game over:")
    print(deck)
    current_multiplier = len(deck[winner])
    print("Multi", current_multiplier)
    score = 0
    while len(deck[winner]) > 0:
        card = deck[winner].pop(0)
        score += card * current_multiplier
        current_multiplier -= 1
    print(score)

    return score

def recursive_game(deck, depth=0):
    # print(depth, deck)

    saved_states = []
    while len(deck[0]) > 0 and len(deck[1]) > 0:
        if deck in saved_states:
            # print("Duplicative state!")
            return 0
        saved_states.append(copy.deepcopy(deck))
        # print("Saved: ", saved_states)
        play_0 = deck[0].pop(0)
        play_1 = deck[1].pop(0)

        winner = None
        if play_1 <= len(deck[1]) and play_0 <= len(deck[0]):
            winner = recursive_game(
                [deck[0].copy()[:play_0], deck[1].copy()[:play_1]],
                depth+1
            )
        elif play_1 > play_0:
            winner = 1
        elif play_0 > play_1:
            winner = 0

        if winner == 1:
            deck[1].append(play_1)
            deck[1].append(play_0)
        elif winner == 0:
            deck[0].append(play_0)
            deck[0].append(play_1)
        # print(depth, deck)
    if depth == 0:
        print("Victory state:", depth, deck)
    if len(deck[0]) > 0:
        return 0
    return 1

def other_winner(winner):
    if winner == 0:
        return 1
    return 0

if __name__ == "__main__":
    assert part2("test.txt") == 291
    assert part2("test2.txt") == 105
    assert part2("input.txt") == 34424
    # not 306
    # not 278
    # not 35572
