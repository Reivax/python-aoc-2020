
def convert_to_dict(lines):
    print("Converting:")
    print(lines)
    ret = {}
    for line in lines:
        kvps = line.split(' ')
        for kvp in kvps:
            kv = kvp.split(':', 1)
            ret[kv[0].strip()] = kv[1].strip()
    return ret

def validate_pport(pport):
    print("vaidating:", pport)
    mandatory_fields = [
        "byr",
        "iyr",
        "eyr",
        "hgt",
        "hcl",
        "ecl",
        "pid",
        # "cid",
    ]
    valid_fields = 0
    for mfield in mandatory_fields:
        if pport.get(mfield, None):
            valid_fields += 1
    if valid_fields >= 7:
        print("Accepted, so far")
        return subvalidate(pport)
    print("Rejected")
    return False

def subvalidate(pport):
    try:
        val = pport.get("byr", "0")
        if len(val) != 4:
            print("Invalid BYR {}".format(val))
            return False
        if int(val) < 1920 or int(val) > 2002:
            print("Invalid BYR {}".format(val))
            return False

        val = pport.get("iyr", "0")
        if len(val) != 4:
            print("Invalid iyr {}".format(val))
            return False
        if int(val) < 2010 or int(val) > 2020:
            print("Invalid iyr{}".format(val))
            return False

        val = pport.get("eyr", "0")
        if len(val) != 4:
            print("Invalid eyr {}".format(val))
            return False
        if int(val) < 2020 or int(val) > 2030:
            print("Invalid eyr{}".format(val))
            return False

        val = pport.get("hgt", "xxxx")
        if val[-2:] == "cm":
            if int(val[0:3]) < 150 or int(val[0:3]) > 193:
                print("Invalid cm hgt {}, {}".format(val, int(val[0:3])))
                return False
        elif val[-2:] == "in":
            if int(val[0:2]) < 59 or int(val[0:2]) > 76:
                print("Invalid in hgt {}, {}".format(val, int(val[0:2])))
                return False
        else:
            print(val[-2:])
            print("hgt wasnt in or cm {}".format(val))
            return False

        val = pport.get("hcl", "0")
        if val[0] == "#" and len(val) == 7:
            for c in val[1:]:
                if ord(c) < ord('0') or ord(c) > ord('f'):
                    print("Invalid hcl {}".format(val))
                    return False
        else:
            print("Invalid hcl {}".format(val))
            return False


        val = pport.get("ecl", None)
        if val not in ["amb","blu","brn","gry","grn","hzl","oth"]:
            print("INvalid ecl: {}".format(val))
            return False

        val = pport.get("pid", "0")
        if len(val) == 9:
            for c in val:
                if ord(c) < ord('0') or ord(c) > ord('9'):
                    print("Invalid pid {}".format(val))
                    return False
        else:
            print("Invalid pid {}".format(val))
            return False
    except ValueError as e:
        print("Excepted")
        print(e)
        return False
    return True



if __name__ == "__main__":
    with open("input.txt") as inf:
        lines = []
        valid_count = 0
        for line in inf:
            line = line.strip()
            if len(line.strip()):
                lines.append(line)
            else:
                pport = convert_to_dict(lines)
                if validate_pport(pport):
                    valid_count += 1
                lines = []
        print("Valid count: {}".format(valid_count))
