import json

def rule_dict_maker():
    forward_rules = {}
    for rule_line in open("input.txt"):
        rule_line = rule_line.strip()
        outer, inner_bags = rule_line.split("bags contain")
        outer = outer.strip()
        if "no other bags" in inner_bags:
            forward_rules[outer] = []
            continue
        inner_bags_string_list = inner_bags.split(', ')
        inner_bags_list = []
        for inner_bag in inner_bags_string_list:
            inner_bag: str = inner_bag.strip().strip('.')
            if inner_bag.endswith('s'):
                inner_bag = inner_bag[:-4]
            else:
                inner_bag = inner_bag[:-3]
            (inner_bag_qty, inner_bag_desc) = inner_bag.split(' ', 1)
            inner_bag_desc = inner_bag_desc.strip()
            inner_bag_qty = int(inner_bag_qty)
            inner_bags_list.append((inner_bag_qty, inner_bag_desc))
        forward_rules[outer] = inner_bags_list
    return forward_rules

def what_bag_could_contain(forward_rules, target_bag):
    ret = [target_bag]
    # print("Looking for the parent of ", target_bag)
    for k, values in forward_rules.items():
        for qty, bag_desc in values:
            if bag_desc == target_bag:
                ret.extend(what_bag_could_contain(forward_rules, k))
    return ret

def how_many_bags_within(forward_rules, target_bag, accum=0):
    for inner_bag_pair in forward_rules[target_bag]:
        (inner_qty, inner_desc) = inner_bag_pair
        # ret.append(inner_qty)
        for i in range(inner_qty):
            accum = how_many_bags_within(forward_rules, inner_desc, accum+1)
    return accum


if __name__ == "__main__":
    forward_rules = rule_dict_maker()
    # print(json.dumps(forward_rules, indent='\t'))

    part1 = what_bag_could_contain(forward_rules, "shiny gold")
    part1.remove("shiny gold")
    part1 = len(set(part1))
    print(part1)
    assert part1 == 335

    part2 = how_many_bags_within(forward_rules, "shiny gold")
    print(part2)
    assert part2 == 2431
