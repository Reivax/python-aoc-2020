def debug(*args, **kwargs):
    # print(*args, **kwargs)
    pass


def find_slope(right, down=1):
    # with open("input.txt", "r") as f:
    with open("input_chase.txt", "r") as f:
        print("Right {}  Down {}".format(right, down))
        trees = 0
        for idx, line in enumerate(f):
            if not idx:
                continue
            if idx % down:
                # debug("skipped")
                continue
            stripped_line = line.strip()
            i = int(idx*right/down) % (len(stripped_line))
            # line_list = list(stripped_line)
            if line[i] == "#":
                trees += 1
            #     line_list[i] = "X"
            # else:
            #     line_list[i] = "O"
            # debug(''.join(line_list))
        print("Trees: ", trees)
        return trees


if __name__ == "__main__":
        prod = find_slope(1) * find_slope(3) * find_slope(5) * find_slope(7) * find_slope(1, 2)
        print("prod", prod, "1355323200")
