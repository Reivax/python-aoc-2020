def find_row(line):
    row = 0
    if line[0] == "B": row += 64
    if line[1] == "B": row += 32
    if line[2] == "B": row += 16
    if line[3] == "B": row += 8
    if line[4] == "B": row += 4
    if line[5] == "B": row += 2
    if line[6] == "B": row += 1
    return row

def find_col(line):
    col = 0
    if line[7] == "R": col += 4
    if line[8] == "R": col += 2
    if line[9] == "R": col += 1
    return col


def main1():
    assert find_row("BFFFBBFRRR") == 70
    assert find_row("FFFBBBFRRR") == 14
    assert find_row("BBFFBBFRLL") == 102
    assert find_col("BFFFBBFRRR") == 7
    assert find_col("FFFBBBFRRR") == 7
    assert find_col("BBFFBBFRLL") == 4
    max_id = 0
    every_id = []
    with open("input.txt") as inf:
        for line in inf:
            row = find_row(line)
            col = find_col(line)
            max_id = max(max_id, row*8+col)
            every_id.append(row*8+col)
    print("Max id: {}".format(max_id))
    every_id.sort()
    missing_ids = []
    for i in range(min(every_id), max(every_id)):
        if i not in every_id:
            missing_ids.append(i)
    print(missing_ids)

def main2():
    ids = [int(line.strip().translate(str.maketrans("BFRL","1010")), 2) for line in open("input.txt")]
    print("Max ID: {}; Missing seat {}".format(max(ids), [x for x in range(min(ids), max(ids)) if x not in ids and x+1 in ids and x-1 in ids] ))


if __name__ == "__main__":
    main1()
    main2()
