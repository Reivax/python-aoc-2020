s = set(int(l.translate(str.maketrans("BFRL", "1010")), 2) for l in open("input.txt"))
print(f"Max ID: {max(s)}; Missing seat {[set(range(min(s), max(s)))-s][0]}")
