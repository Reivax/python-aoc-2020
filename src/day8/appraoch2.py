import copy
def parse_line(line):
    instr = line[0:3]
    dir = line[4]
    amt = line[5:]
    # print("Instr: ", instruction, "  Dir", direction, "  amt ", amt)
    return instr, dir, int(amt)

class Executor(object):
    def __init__(self, lines=None):
        self.instr_idx = 0
        self.accumulator = 0
        self.program = []
        self.line_counter = {}
        if lines:
            self.construct_program_from_lines(lines)

    def construct_program_from_lines(self, lines):
        for line in lines:
            self.program.append(parse_line(line))

    def execute_program(self):
        """

        :return: True if we went off the end.
        """
        while True:
            # print("Executing line", self.instr_idx)
            if self.line_counter.get(self.instr_idx, 0) >= 1:
                print("Double-tapped an instruction.")
                return False
            else:
                self.line_counter[self.instr_idx] = self.line_counter.get(self.instr_idx, 0)+1
            self.execute_line(*self.program[self.instr_idx])
            if self.instr_idx >= len(self.program):
                print("Went off the end!")
                return True

    def execute_line(self, cmd, dir, amt):
        print("Insr Idx ", self.instr_idx, "  Instr ", cmd, "  Dir ", dir, "  amt ", amt)
        if cmd == "nop":
            self.instr_idx += 1
        elif cmd == "acc":
            if dir == "+":
                self.accumulator += amt
            else:
                self.accumulator -= amt
            self.instr_idx += 1
        elif cmd == "jmp":
            if dir == "+":
                self.instr_idx += amt
            else:
                self.instr_idx -= amt

def swap_one_cmd(lines):
    currently_amended_line = -1
    while True:
        currently_amended_line += 1
        print("Applying fix on line ", currently_amended_line)
        (cmd, dir, amt) = parse_line(lines[currently_amended_line])
        if cmd == "jmp":
            cmd = "nop"
        elif cmd == "nop":
            cmd = "jmp"
        else:
            continue
        executor = Executor(lines)
        executor.program[currently_amended_line] = (cmd, dir, amt)
        went_off_end = executor.execute_program()
        if went_off_end:
            return executor.accumulator


def part1():
    lines = [l.strip() for l in open("input.txt")]
    print(len(lines))
    assert len(lines) == 601
    exe = Executor(lines)
    exe.execute_program()
    print(exe.accumulator)
    assert exe.accumulator == 1384

def part2():
    lines = [l.strip() for l in open("input.txt")]
    print(len(lines))
    assert len(lines) == 601
    ctr = swap_one_cmd(lines)
    print(ctr)
    assert ctr == 761

def test():
    lines = [l.strip() for l in open("test.txt")]
    print(len(lines))
    assert len(lines) == 9
    exe = Executor(lines)
    exe.execute_program()
    print(exe.accumulator)
    assert exe.accumulator == 5

    ctr = swap_one_cmd(lines)
    print(ctr)
    assert ctr == 8

if __name__ == "__main__":
    part2()
