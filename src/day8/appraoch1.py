def parse_line(line):
    instruction = line[0:3]
    direction = line[4]
    amt = line[5:]
    # print("Instr: ", instruction, "  Dir", direction, "  amt ", amt)
    return (instruction, direction, int(amt))

def execute(lines):
    accumulator = 0
    instr_idx = 0
    already_run = {}
    double_execute = False
    while True:
        line = lines[instr_idx]
        (instruct, direction, amt) = parse_line(line)
        # print("Insr Idx ", instr_idx, "  Instr ", instruct, "  Dir ", direction, "  amt ", amt)
        if already_run.get(instr_idx):
            double_execute = True
            break
        else:
            already_run[instr_idx] = True

        if instruct == "nop":
            instr_idx += 1
            continue
        elif instruct == "acc":
            if direction == "+":
                accumulator += amt
            else:
                accumulator -= amt
            instr_idx += 1
        elif instruct == "jmp":
            if direction == "+":
                instr_idx += amt
            else:
                instr_idx -= amt
        if instr_idx >= len(lines):
            break
    return (accumulator, double_execute)

def swap_one_cmd(lines):
    currently_amended_line = -1
    double_execute = True
    ctr = 0
    while double_execute:
        currently_amended_line += 1
        print("Fix on line ", currently_amended_line)
        line = lines[currently_amended_line]
        (cmd, dir, amt) = parse_line(line)
        if cmd == "jmp":
            line = "nop "+dir+str(amt)
        elif cmd == "nop":
            line = "jmp "+dir+str(amt)
        else:
            continue
        copied = lines.copy()
        copied[currently_amended_line] = line
        (ctr, double_execute) = execute(copied)
    return ctr

def test():
    lines = []
    for line in open("test.txt"):
        lines.append(line.strip())
    print(len(lines))
    (ctr, _) = execute(lines)
    print(ctr)
    assert ctr == 5
    ctr = swap_one_cmd(lines)
    print(ctr)
    assert ctr == 8

def part1_2():
    lines = []
    for line in open("input.txt"):
        lines.append(line.strip())
    print(len(lines))
    (ctr, _) = execute(lines)
    print(ctr)
    assert ctr == 1384
    ctr = swap_one_cmd(lines)
    print(ctr)
    assert ctr == 761

if __name__ == "__main__":
    test()
