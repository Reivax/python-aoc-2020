def generate_addresses(mask, memory_address):
    ret = ["0"] * 36
    floating_bit_count = 0
    memory_address_as_str = "{:036b}".format(int(memory_address))
    for idx in range(36):
        if mask[idx] == 'X':
            ret[idx] = '{}'
            floating_bit_count += 1
        else:
            ret[idx] = str(int(mask[idx]) | int(memory_address_as_str[idx]))
    floating_address = "".join(ret)

    for floater in range(2 ** floating_bit_count):
        # floater = bin(floater)[2:].zfill(ctr)
        floater = "{:b}".format(floater).zfill(floating_bit_count)
        decoded_addr = floating_address.format(*floater)
        # print("applied mask    : ", mask)
        # print("Decoded addr str: ", decoded_addr)
        # print("Decoded addr int: ", int(decoded_addr, 2))
        yield int(decoded_addr, 2)

def part2(input_file):
    memory = {}
    current_mask = "x"*36

    with open(input_file) as f:
        for line in f:
            line = line.strip()
            (left, right) = [x.strip() for x in line.split('=', 1)]
            if left == "mask":
                current_mask = right
                # print("New mask: ", current_mask)
            else:
                mem_addr = left[4:-1]
                # print("Mem addr", mem_addr, "Value", right)
                for addr in generate_addresses(current_mask, mem_addr):
                    memory[addr] = int(right)

    return sum(memory.values())


if __name__ == "__main__":
    p2 = part2("test2.txt")
    print(p2)
    assert p2 == 208
    p2 = part2("input.txt")
    print(p2)
    assert p2 == 4160009892257
