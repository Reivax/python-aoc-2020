def mask_value(mask, input_value):
    inpt_as_string = "{:036b}".format(int(input_value))
    # print("Input value:", input_value,  "As str: ", inpt_as_string)
    ret = ['0']*36
    for idx in range(len(mask)):
        # print('IDX:  {},  IPT {},  MASK {}'.format(idx, inpt_as_string[idx], mask[idx]))
        if mask[idx] == 'X':
            ret[idx] = inpt_as_string[idx]
        elif mask[idx] == '1':
            ret[idx] = '1'
        elif mask[idx] == '0':
            ret[idx] = '0'
        else:
            assert False
    print("Input:     ", input_value)
    print("In Binary: ", inpt_as_string)
    print("Mask     : ", mask)
    print("Result   : ", ''.join(ret))
    ret = int(''.join(ret), 2)
    print("Result   : ", ret)
    return ret


def part1(input_file):
    memory = {}
    current_mask = "x"*36

    with open(input_file) as f:
        for line in f:

            line = line.strip()
            (left, right) = line.split('=')
            left = left.strip()
            right = right.strip()

            if left == "mask":
                current_mask = right
                print("New mask: ", current_mask)
            else:
                mem_addr = left[4:-1]
                print("Mem addr", mem_addr, "Value", right)
                memory[mem_addr] = mask_value(current_mask, right)

    print(memory)
    ctr = 0
    for v in memory.values():
        ctr += v
    return ctr



if __name__ == "__main__":
    p1 = part1("test.txt")
    print(p1)
    assert p1 == 165
    p1 = part1("input.txt")
    print(p1)
    assert p1 == 17481577045893
