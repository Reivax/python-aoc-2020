def g(m, a):
    c=sum(1for x in m if x=='X')
    a="".join(['{}'if l=='X'else str(int(l)|int(r))for l,r in zip(m,f"{int(a):036b}")])
    for b in range(2**c):yield int(a.format(*("{:0"+str(c)+"b}").format(b)),2)

def part2(n):
    s={};m="x"
    for l in open(n):
        l,r=[x.strip()for x in l.split('=')]
        if l=="mask":m=r
        else:
            for a in g(m,l[4:-1]):s[a]=int(r)
    return sum(s.values())

if __name__ == "__main__":
    p2 = part2("test2.txt")
    print(p2)
    assert p2 == 208
    p2 = part2("input.txt")
    print(p2)
    assert p2 == 4160009892257
