
def check_for_solution(target, window):
    # FIXME: Should have done 2SUM, but this is fast enough.
    for lidx, l in enumerate(window[:]):
        for ridx, r in enumerate(window[lidx:]):
            if l+r == target:
                return True
    return False

def part_1(window_size, parsed):
    offset = window_size
    print("Lines: ", len(parsed))
    window = parsed[offset-window_size:offset]
    while True:
        target = parsed[offset]
        if not check_for_solution(target, window):
            print("Did not match, line: {}  value: {}".format(offset, target))
            return target
        offset+=1
        if offset >= len( parsed):
            break
        window = parsed[offset-window_size:offset]

def part_2(target, parsed):
    print("Lines: ", len(parsed))
    lidx = 0
    ridx = 0
    while True:
        while sum(parsed[lidx:ridx]) < target:
            ridx += 1
        while sum(parsed[lidx:ridx]) > target:
            lidx += 1
            while lidx > ridx:
                ridx += 1
        if sum(parsed[lidx:ridx]) == target:
            print("Located: ", min(parsed[lidx:ridx]) + max(parsed[lidx:ridx]), parsed[lidx:ridx])
            ridx+=1



if __name__ == "__main__":
    parsed = []
    for line in open(input_file):
        parsed.append(int(line))
    target = part_1(25, parsed)
    print(target)
    assert target == 393911906
    target = 393911906
    sumval = part_2(target, parsed)
    # print(sumval)
    # assert sumval == 59341885

