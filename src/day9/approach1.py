
def check_for_solution(target, window):
    # FIXME: Should have done 2SUM, but this is fast enough.
    for lidx, l in enumerate(window[:]):
        for ridx, r in enumerate(window[lidx:]):
            if l+r == target:
                return True
    return False

def part_1(window_size, input_file):
    # window_size = 5
    offset = window_size
    parsed = []
    for line in open(input_file):
        parsed.append(int(line))
    print("Lines: ", len(parsed))
    window = parsed[offset-window_size:offset]
    while True:
        target = parsed[offset]
        if not check_for_solution(target, window):
            print("Did not match, line: {}  value: {}".format(offset, target))
            return target
        offset+=1
        if offset >= len( parsed):
            break
        window = parsed[offset-window_size:offset]

def part_2(target, input_file):
    # window_size = 5
    parsed = []
    for line in open(input_file):
        parsed.append(int(line))
    print("Lines: ", len(parsed))
    for l in range(len(parsed)):
        for r in range(l, len(parsed)):
            subset = parsed[l:r]
            # print(subset)
            sum_val = sum(subset)
            if sum_val == target:
                print("Got em", subset)
                return min(subset) + max(subset)
            if sum_val > target:
                break


if __name__ == "__main__":
    # part_1(5, "test.txt")
    target = part_1(25, "input.txt")
    print(target)
    assert target == 393911906
    target = 393911906
    sumval = part_2(target, "input.txt")
    print(sumval)
    assert sumval == 59341885

