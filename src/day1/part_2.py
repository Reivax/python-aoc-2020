if __name__ == "__main__":
    numbers = []
    with open("input.txt", "r") as f:
        for line in f:
            numbers.append(int(line))

    for lidx, left in enumerate(numbers):
        for cidx, center in enumerate(numbers[lidx:]):
            for ridx, right in enumerate(numbers[lidx+cidx:]):
                if left + center + right == 2020:
                    print(left, center, right, left*center*right)
