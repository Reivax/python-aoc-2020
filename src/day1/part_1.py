if __name__ == "__main__":
    numbers = []
    with open("input.txt", "r") as f:
        for line in f:
            numbers.append(int(line))

    for lidx, left in enumerate(numbers):
        for ridx, right in enumerate(numbers[lidx:]):
            if left + right == 2020:
                print(left, right, left*right)
