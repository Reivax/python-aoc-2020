import typing

def load(input) -> (typing.List[int], int):
    ret = []
    for c in input.strip():
        ret.append(int(c))
    return ret, max(ret)

def print_cups(cups, current_value):
    return ' '.join([str(cup)  if cup != current_value else str("(" + str(cup) + ")") for cup in cups])

def iterate(next, n):
    _min = 1
    _max = 1_000_000
    target = n - 1
    a = next[n]
    b = next[a]
    c = next[b]
    if target < _min:
        target = _max
    while target in [a,b,c]:
        target -= 1
        if target < _min:
            target = _max
    next[n] = next[c]
    next[c] = next[target]
    next[target] = a

def part2(input):
    cups, max_cup = load(input)
    next = [*range(0, 1_000_001)]
    for idx in range(len(cups)-1):
        if not idx%1000:
            print("idx", idx, "cups[idx]", cups[idx])
        next[cups[idx]] = cups[idx+1]
    print("Next: ", next[0:50])
    next[1_000_000] = cups[0]

    i = cups[0]
    for x in range(10_000_000):
        if not x%1000:
            print("Turn", x)
        iterate(next, i)
        i = next[i]
    return next[i] * next[next[i]]


if __name__ == "__main__":
    print(part2("389125467"))
    # print(part2("418976235"))
