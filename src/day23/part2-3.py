import itertools
import typing

def load(input) -> (typing.List[int], int):
    ret = []
    for c in input.strip():
        ret.append(int(c))
    return ret

class LLNode:
    def __init__(self, val):
        self.val = val
        self.right: typing.Optional[LLNode] = None

    def __repr__(self):
        return repr(self.val)


def part2(input, turns=10_000_000):
    cups_raw = load(input)
    return solve_for_state(itertools.chain(cups_raw, range(10, 1_000_001)), turns)

def solve_for_state(initial_state, turns=10_000_000):
    max_element = None
    node_lookup = {}
    previous_node = None
    first_node = None
    node_one = None
    current_node = None
    for cup in initial_state:
        current_node = LLNode(cup)
        if previous_node:
            previous_node.right = current_node
            max_element = max(max_element, cup)
        else:
            first_node = current_node
            max_element = cup
        if cup == 1:
            node_one = current_node
        node_lookup[cup] = current_node
        previous_node = current_node
    current_node.right = first_node
    current_node = first_node
    for idx in range(turns):
        p1: LLNode = current_node.right
        # ok so, we want to take the next
        p2: LLnode = p1.right
        p3: LLnode = p2.right
        current_node.right = p3.right

        dest_val = current_node.val - 1
        if dest_val < 1:
            dest_val += max_element
        while dest_val in (p1.val, p2.val, p3.val):
            dest_val -= 1
            if dest_val < 1:
                dest_val = max_element
        dest_node = node_lookup[dest_val]
        p3.right = dest_node.right
        dest_node.right = p1
        current_node = current_node.right
    print(node_one.right.val, node_one.right.right.val, node_one.right.val * node_one.right.right.val)
    return node_one.right.val * node_one.right.right.val


if __name__ == "__main__":
    assert part2("389125467") == 149245887792
    assert part2("418976235") == 563362809504
    # Not 149245887792
    # print(part2("418976235"))
