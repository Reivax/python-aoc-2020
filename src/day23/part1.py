import typing

def load(input) -> (typing.List[int], int):
    ret = []
    for c in input.strip():
        ret.append(int(c))
    return ret, max(ret)

def print_cups(cups, current_value):
    return ' '.join([str(cup)  if cup != current_value else str("(" + str(cup) + ")") for cup in cups])

def pick_up_after_current(cups, current_value):
    ret = []
    idx = cups.index(current_value) + 1
    for _ in range(3):
        if idx >= len(cups):
            idx = 0
        # print("About to pop idx {}, return {}, current {}".format(idx, ret, print_cups(cups, current_value)))
        ret.append(cups.pop(idx))
    print("Picked up", ret)
    return ret

def find_next_lowest(cups, current_value):
    next_lowest = current_value - 1
    print("Looking for index of", next_lowest,"in", print_cups(cups, current_value))
    while next_lowest not in cups:
        next_lowest -= 1
        if next_lowest <= 0:
            next_lowest = len(cups) + 3
        print("Looking again index of", next_lowest,"in", print_cups(cups, current_value))
    ret = next_lowest
    print("Located index of", next_lowest,"in", print_cups(cups, next_lowest))
    return ret

def calculate_next_current(cups, current_value):
    current_idx = cups.index(current_value)
    current_idx += 1
    current_idx %= len(cups)
    print("Current idx", current_idx, "len", len(cups))
    return cups[current_idx]

def rotate_to_fit(cups, current_value, round):
    while cups.index(current_value) != (round % len(cups)):
        cup = cups.pop(0)
        cups.append(cup)

def rotate_for_solution(cups):
    while cups.index(1) != 0:
        cup = cups.pop(0)
        cups.append(cup)

    print("Cups state at solutoin", print_cups(cups, 1))
    cups.pop(0)
    return ''.join(str(x) for x in cups)

def part1(input, moves):
    cups, max_cup = load(input)
    current_value = cups[0]
    for round in range(1, moves+1):
        print("Round", round)
        print(print_cups(cups, current_value))
        picked_up = pick_up_after_current(cups, current_value)
        destination_value = find_next_lowest(cups, current_value)
        destination_index = cups.index(destination_value)
        print("Destination value", destination_value)
        for picked_up_cup in picked_up[::-1]:
            print("Adding '", picked_up_cup, "' to index", destination_index, "right after", destination_value, ":", print_cups(cups, current_value))
            cups.insert((destination_index+1), picked_up_cup)
            print("Added '", picked_up_cup, "' to ", print_cups(cups, current_value))
        current_value = calculate_next_current(cups, current_value)
        rotate_to_fit(cups, current_value, round)
    ret = rotate_for_solution(cups)
    print(ret)
    return ret


if __name__ == "__main__":
    assert part1("389125467", 10) == "92658374"
    assert part1("389125467", 100) == "67384529"
    assert part1("418976235", 100) == "96342875"
