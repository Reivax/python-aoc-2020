class Card:
    def __init__(self, v):
        self.v = v
        self.n = None


with open("input.txt") as f:
    nums = [int(x) for x in f.readline().strip()]

for x in range(len(nums), 1000000):
    nums.append(x + 1)

cards = {}

prev = Card(None)
for num in nums:
    cards[num] = Card(num)
    prev.n = cards[num]
    prev = cards[num]

prev.n = cards[nums[0]]
curr = cards[nums[0]]

for i in range(10000000):
    pick = curr.n
    end = pick.n.n
    curr.n = end.n

    dest = curr
    while dest in (curr, pick, pick.n, end):
        dest = cards[(dest.v - 2) % len(nums) + 1]

    end.n = dest.n
    dest.n = pick
    curr = curr.n

    if i % 10000 == 0:
        print(i)

print(cards[1].n.v * cards[1].n.n.v)
