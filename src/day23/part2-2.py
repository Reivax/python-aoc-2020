import typing

def load(input) -> (typing.List[int], int):
    ret = []
    for c in input.strip():
        ret.append(int(c))
    return ret

class LLNode:
    def __init__(self, val):
        self.val = val
        next_: LLNode = None

    def set_behind(self, behind):
        behind.next_ = self

    def __repr__(self):
        return repr(self.val)


def part2(input):
    cups = []
    indexes = {}
    for cup in load(input):
        cups.append(LLNode(cup))
        indexes[cup] = cups[-1]
    for i in range(10, 1_000_001):
        cups.append(LLNode(i))
        indexes[i] = cups[-1]

    prev = cups[-1]
    for cup in cups:
        cup.set_behind(prev)
        prev = cup

    current = cups[0]
    for _ in range(10_000_000):
        # ok so, we want to take the next 3
        p1 = current.next_
        p2 = p1.next_
        p3 = p2.next_

        # now then, we do some.... magic
        val = current.val - 1
        while val in (p1.val, p2.val, p3.val) or val < 1:
            if val > 1:
                val -= 1
            else:
                val = 1_000_000

        if val == current.val:
            current = p1
        else:
            # :^)
            temp1 = indexes[val].next_
            previous = current
            current = p3.next_

            # unsafe
            indexes[val].next_ = p1
            p3.next_ = temp1
            previous.next_ = current


    while current.val != 1:
        current = current.next_

    print(current.next_.val, current.next_.next_.val)
    return current.next_.val * current.next_.next_.val


if __name__ == "__main__":
    assert part2("389125467") == 149245887792
    print(part2("418976235"))
    # Not 149245887792
    # print(part2("418976235"))
