import typing
import json

def load(filename):
    allergen_set = set()
    food_set = set()
    all_lines = []
    for line in open(filename):
        food_line, allergen_line = [x.strip() for x in line.split("(contains")]
        allergen_line = allergen_line[:-1]
        allergens = [x.strip() for x in allergen_line.split(',')]
        foods = [x.strip() for x in food_line.split(' ')]
        for allergen in allergens:
            allergen_set.add(allergen)
        for food in foods:
            food_set.add(food)
        all_lines.append((foods, allergens))

    return allergen_set, food_set, all_lines

def part2(filename):
    allergen_set, food_set, all_lines = load(filename)
    allergen_map = {x: allergen_set.copy() for x in food_set}
    for line_foods, line_allergens in all_lines:
        for food in (food_set - set(line_foods)):
            allergen_map[food] -= set(line_allergens)
    safe_foods = {k for k,v in allergen_map.items() if len(v) == 0}
    print(safe_foods)

    working = True
    print(allergen_map)
    while working:
        for food, candidate_allergen in allergen_map.items():
            # If any allergen stil has two food candidates, keep going.
            if len(candidate_allergen) > 1:
                break
        else:
            working = False
            continue # Continue the while loop.

        for candidate_food, allergen in allergen_map.items():
            if len(allergen) == 1:
                print("Food {} has only one allergen posissble, {}, removing it from everwhere else.".format(
                    candidate_food, allergen
                ))
                for noncandidate_food, impossible_allergens in allergen_map.items():
                    if noncandidate_food == candidate_food:
                        continue
                    impossible_allergens -= allergen
    print(allergen_map)
    minimal_allergen_map = {}
    for k, v in allergen_map.items():
        if len(v) == 1:
            minimal_allergen_map[k] = list(v)[0]
    print(minimal_allergen_map)
    sorted_list = sorted(minimal_allergen_map.values())
    rev_allergens_map = {v:k for k,v in minimal_allergen_map.items()}
    ret = ','.join(rev_allergens_map.get(x) for x in sorted_list)
    print(ret)
    return ret

if __name__ == "__main__":
    assert part2("test.txt") == "mxmxvkd,sqjhc,fvjkl"
    assert part2("input.txt") == "mfp,mgvfmvp,nhdjth,hcdchl,dvkbjh,dcvrf,bcjz,mhnrqp"
