def load(filename):
    allergen_set = set()
    food_set = set()
    all_lines = []
    for line in open(filename):
        food_line, allergen_line = [x.strip() for x in line.split("(contains")]
        allergen_line = allergen_line[:-1]
        allergens = [x.strip() for x in allergen_line.split(',')]
        foods = [x.strip() for x in food_line.split(' ')]
        for allergen in allergens:
            allergen_set.add(allergen)
        for food in foods:
            food_set.add(food)
        all_lines.append((foods, allergens))

    return allergen_set, food_set, all_lines

def part1(filename):
    allergen_set, food_set, all_lines = load(filename)
    allergen_dict = {}
    for ingredient_line, allergen_line in all_lines:
        for allergen in allergen_line:
            list_of_sets_of_ingredients = allergen_dict.get(allergen, [])
            list_of_sets_of_ingredients.append(set(ingredient_line))
            allergen_dict[allergen] = list_of_sets_of_ingredients
    print("Every allergen, and every food item they appear with", allergen_dict)
    intersected_allergen_dict = {}
    for allergen, list_of_sets_of_ingredients in allergen_dict.items():
        intersected_allergen_dict[allergen] = set.intersection(*list_of_sets_of_ingredients)
    print("Every allergen, and every candidate ingredient", intersected_allergen_dict)
    unsafe_ingredients = set()
    for allergen, ingredient_set in intersected_allergen_dict.items():
        unsafe_ingredients = unsafe_ingredients.union(ingredient_set)
    print("Every ingredient that is unsafe to eat", unsafe_ingredients)
    safe_foods = food_set.difference(unsafe_ingredients)
    print("Every ingredient that is allergen free", safe_foods)
    ctr = 0
    for ingredient_line, allergen_line in all_lines:
        for ingredient  in ingredient_line:
            if ingredient in safe_foods:
                ctr += 1
    print("Total occurrences of every allergen-free ingredient", ctr)
    return ctr


if __name__ == "__main__":
    assert part1("test.txt") == 5
    assert part1("input.txt") ==  2412
