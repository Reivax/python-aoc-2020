def load(filename):
    allergen_set = set()
    food_set = set()
    all_lines = []
    for line in open(filename):
        food_line, allergen_line = [x.strip() for x in line.split("(contains")]
        allergen_line = allergen_line[:-1]
        allergens = [x.strip() for x in allergen_line.split(',')]
        foods = [x.strip() for x in food_line.split(' ')]
        for allergen in allergens:
            allergen_set.add(allergen)
        for food in foods:
            food_set.add(food)
        all_lines.append((foods, allergens))

    return allergen_set, food_set, all_lines

def part1(filename):
    allergen_set, food_set, all_lines = load(filename)
    allergen_map = {x: allergen_set.copy() for x in food_set}
    for line_foods, line_allergens in all_lines:
        for food in (food_set - set(line_foods)):
            allergen_map[food] -= set(line_allergens)
    safe_foods = {k for k,v in allergen_map.items() if len(v) == 0}
    print(safe_foods)
    ret = 0
    for line_foods, line_allergens in all_lines:
        for food in line_foods:
            if food in safe_foods:
                ret += 1
    print(ret)
    return ret

if __name__ == "__main__":
    assert part1("test.txt") == 5
    assert part1("input.txt") ==  2412
