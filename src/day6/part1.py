def count_yeses(lines):
    lines = ' '.join(lines)
    print("Group", lines)
    my_dict = {}
    for c in lines:
        if ord('a') <= ord(c) <= ord('z'):
            my_dict[c] = True
    print(my_dict.keys())
    sum_of_counts = len(my_dict.keys())
    print(sum_of_counts)
    return sum_of_counts

if __name__ == "__main__":

    with open("input.txt") as inf:
        lines = []
        valid_count = 0
        for line in inf:
            line = line.strip()
            if len(line.strip()):
                lines.append(line)
            else:
                valid_count += count_yeses(lines)
                lines = []
        print(valid_count)