def multiline_read(filename):
    with open(filename) as inf:
        lines = []
        for line in inf:
            line = line.strip()
            if len(line):
                lines.append(line)
            else:
                yield lines
                lines = []
        yield lines

def count_yeses(lines):
    global global_count
    print("Group", lines)
    d = {}
    for line in lines:
        for c in set(line):
            if ord('a') <= ord(c) <= ord('z'):
                d[c] = d.get(c, 0) + 1
    all_yes = 0
    for k in d.keys():
        if d.get(k) == len(lines):
            all_yes += 1
    print(all_yes)
    return all_yes


if __name__ == "__main__":
    global_count = 0
    for group in multiline_read("input.txt"):
        global_count += count_yeses(group)
    assert global_count == 3476

    global_count = 0
    for group in multiline_read("shattered_sunslight.txt"):
        global_count += count_yeses(group)
    print(global_count)
    assert global_count == 39

    global_count = 0
    for group in multiline_read("shattered_sunslight2.txt"):
        global_count += count_yeses(group)
    print(global_count)
    assert global_count == 3640
