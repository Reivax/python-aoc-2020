import functools
def load(input_file):
    with open(input_file) as f:
        return load_lines(f.read())

def load_lines(lines):
    parsed = []
    for line in lines.splitlines():
        row = [c for c in line.strip()]
        # print(row)
        parsed.append(row)
    # print(parsed)
    return parsed

floor = '.'
empty = 'L'
occupied = '#'

def is_cell_occupied(board, row, col):
    if row<0 or col<0 or row>=len(board) or col>=len(board[0]):
        return False
    # print("Row, col", row, col)
    return board[row][col] == occupied

def count_neighbors(board, row, col):
    neighbors = 0
    for row_ in [row-1, row, row+1]:
        for col_ in [col-1, col, col+1]:
            # Cant count yourself..
            if row_ == row and col_ == col:
                continue
            # print("Test of ", row_, col_, is_cell_occupied(board, row_, col_))
            if is_cell_occupied(board, row_, col_):
                neighbors += 1
    return neighbors

def determine_state(board, row, col):
    # if not row and not col:
    #     print("Neighbor count", count_neighbors(board, row, col))
    if board[row][col] == floor:
        return floor
    elif board[row][col] == empty and \
        count_neighbors(board, row, col) == 0:
        return occupied
    elif board[row][col] == occupied and \
        count_neighbors(board, row, col) >= 4:
        return empty
    else:
        return board[row][col]

def part1(input_board, test):
    current_board = input_board.copy()
    width = len(current_board[0])
    height = len(current_board)
    idx = 0
    while True:
        # print_board(current_board)
        print(idx)

        next_board = []
        idx += 1
        for row in range(height):
            next_board.append([])
            for col in range(width):
                next_board[row].append(determine_state(current_board, row, col))
        if current_board == next_board:
            break
        current_board = next_board
    ctr = 0
    for row in range(height):
        for col in range(width):
            if current_board[row][col] == occupied:
                ctr += 1
    print("Solution", ctr)
    return ctr

def print_board(board):
    pass
    # print(board)
    for row in board:
        print(''.join(row))
        # print(' '.join(row).strip())


state = [None] * 7
state[0] = load_lines("""L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL
""")

state[1] = load_lines("""#.##.##.##
#######.##
#.#.#..#..
####.##.##
#.##.##.##
#.#####.##
..#.#.....
##########
#.######.#
#.#####.##
""")

state[2] = load_lines("""#.LL.L#.##
#LLLLLL.L#
L.L.L..L..
#LLL.LL.L#
#.LL.LL.LL
#.LLLL#.##
..L.L.....
#LLLLLLLL#
#.LLLLLL.L
#.#LLLL.##
""")

state[3] = load_lines("""#.##.L#.##
#L###LL.L#
L.#.#..#..
#L##.##.L#
#.##.LL.LL
#.###L#.##
..#.#.....
#L######L#
#.LL###L.L
#.#L###.##
""")

state[4] = load_lines("""#.#L.L#.##
#LLL#LL.L#
L.L.L..#..
#LLL.##.L#
#.LL.LL.LL
#.LL#L#.##
..L.L.....
#L#LLLL#L#
#.LLLLLL.L
#.#L#L#.##
""")

state[5] = load_lines("""#.#L.L#.##
#LLL#LL.L#
L.#.L..#..
#L##.##.L#
#.#L.LL.LL
#.#L#L#.##
..L.L.....
#L#L##L#L#
#.LLLLLL.L
#.#L#L#.##""")



if __name__ == "__main__":
    parsed_input = load("test.txt")
    print("Size:  Row, Col", len(parsed_input), len(parsed_input[0]))
    part1(parsed_input, True)
    parsed_input = load("input.txt")
    print("Size:  Row, Col", len(parsed_input), len(parsed_input[0]))
    part1(parsed_input, False)
