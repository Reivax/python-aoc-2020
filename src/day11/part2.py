import functools
def load(input_file):
    with open(input_file) as f:
        return load_lines(f.read())

def load_lines(lines):
    parsed = []
    for line in lines.splitlines():
        row = [c for c in line.strip()]
        parsed.append(row)
    return parsed

floor = '.'
empty = 'L'
occupied = '#'
def look_to_next_seat(board, row, col, d_row, d_col):
    """

    Return True if visible seat is occupied, False, if empty or out of bound.
    :param board: board layout.
    :param row: current seat row
    :param col: current seat column
    :param d_row: delta row (-1,0,+1)
    :param d_col: delta column (-1,0,+1)
    :return:
    """
    # viewing row = row + delta row
    v_row = row + d_row
    v_col = col + d_col
    while True:
        # out of bounds.
        if v_row<0 or v_col<0 or v_row>=len(board) or v_col>=len(board[0]):
            return False
        elif board[v_row][v_col] == empty:
            return False
        elif board[v_row][v_col] == occupied:
            return True
        # Floors are "invisible"
        elif board[v_row][v_col] == floor:
            v_row += d_row
            v_col += d_col
        else:
            assert False

def count_visible_occupied_seats(board, row, col):
    neighbors = 0
    for d_row in [-1, 0, +1]:
        for d_col in [-1, 0, +1]:
            # Cant count yourself..
            if d_row == 0 and d_col == 0:
                continue
            if look_to_next_seat(board, row, col, d_row, d_col):
                neighbors += 1
    return neighbors

def determine_state(board, row, col):
    visible_occupied_seats = count_visible_occupied_seats(board, row, col)
    # if not row and not col:
    #     print("Neighbor count", visible_occupied_seats)
    if board[row][col] == floor:
        return floor
    elif board[row][col] == empty and \
        visible_occupied_seats == 0:
        return occupied
    elif board[row][col] == occupied and \
        visible_occupied_seats >= 5:
        return empty
    else:
        return board[row][col]

def part2(input_board, test):
    current_board = input_board.copy()
    width = len(current_board[0])
    height = len(current_board)
    idx = 0
    while True:
        if test:
            print_board(current_board)
        print("Generation", idx)

        next_board = []
        idx += 1
        for row in range(height):
            next_board.append([])
            for col in range(width):
                next_board[row].append(determine_state(current_board, row, col))
        if same_board_check(current_board, next_board):
            break
        current_board = next_board
        if idx > 6 and test:
            break
    ctr = 0
    for row in range(height):
        for col in range(width):
            if current_board[row][col] == occupied:
                ctr += 1
    print("Solution", ctr)
    return ctr

def print_board(board):
    pass
    # print(board)
    for row in board:
        print(row)
        # print(' '.join(row).strip())

def same_board_check(board1, board2):
    width = len(board1[0])
    height = len(board1)
    for row in range(height):
        for col in range(width):
            if not board1[row][col] == board2[row][col]:
                return False
    return True


state = [None] * 7
state[0] = load_lines("""L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL
""")

state[1] = load_lines("""#.##.##.##
#######.##
#.#.#..#..
####.##.##
#.##.##.##
#.#####.##
..#.#.....
##########
#.######.#
#.#####.##
""")

state[2] = load_lines("""#.LL.LL.L#
#LLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLL#
#.LLLLLL.L
#.LLLLL.L#
""")

state[3] = load_lines("""#.L#.##.L#
#L#####.LL
L.#.#..#..
##L#.##.##
#.##.#L.##
#.#####.#L
..#.#.....
LLL####LL#
#.L#####.L
#.L####.L#
""")

state[4] = load_lines("""#.L#.L#.L#
#LLLLLL.LL
L.L.L..#..
##LL.LL.L#
L.LL.LL.L#
#.LLLLL.LL
..L.L.....
LLLLLLLLL#
#.LLLLL#.L
#.L#LL#.L#
""")

state[5] = load_lines("""#.L#.L#.L#
#LLLLLL.LL
L.L.L..#..
##L#.#L.L#
L.L#.#L.L#
#.L####.LL
..#.#.....
LLL###LLL#
#.LLLLL#.L
#.L#LL#.L#
""")

state[6] = load_lines("""#.L#.L#.L#
#LLLLLL.LL
L.L.L..#..
##L#.#L.L#
L.L#.LL.L#
#.LLLL#.LL
..#.L.....
LLL###LLL#
#.LLLLL#.L
#.L#LL#.L#
""")



if __name__ == "__main__":
    parsed_input = load("test.txt")
    print("Size:  Row, Col", len(parsed_input), len(parsed_input[0]))
    part2(parsed_input, True)
    parsed_input = load("input.txt")
    print("Size:  Row, Col", len(parsed_input), len(parsed_input[0]))
    part2(parsed_input, False)
