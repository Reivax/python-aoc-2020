def transform(subject_number, loop_size):
    divisor = 20201227
    value = 1
    for _ in range(loop_size):
        value *= subject_number
        value %= divisor
    return value

def identify_loop_size(initial_subject_number, public_key):
    subject_number = initial_subject_number
    divisor = 20201227
    value = 1
    loop_size = 0
    while True:
        loop_size += 1
        value *= subject_number
        value %= divisor
        if value == public_key:
            break
    return loop_size


def part1(card_public_key, door_public_key):
    # door_loop = identify_loop_size(7, door_public_key)
    # print("Door Subject & Loop", door_loop)
    card_loop = identify_loop_size(7, card_public_key)
    print("Card Subject & Loop", card_loop)
    encryption_key = transform(door_public_key, card_loop)
    # encryption_key = transform(card_public_key, door_loop)
    print(encryption_key)
    return encryption_key

if __name__ == "__main__":
    assert part1(5764801, 17807724) == 14897079
    assert part1(7573546, 17786549) == 7032853
    # part1("input.txt")
