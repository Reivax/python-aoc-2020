# NExt time look to https://en.wikipedia.org/wiki/Shunting-yard_algorithm

def load(input_file):
    rules = {}
    messages = []
    segment = 0
    with open(input_file) as f:
        for line in f:
            line = line.strip()
            if len(line) == 0:
                segment += 1
                continue
            if segment == 0:
                ruleid, rule = process_line_rule(line)
                rules[ruleid] = rule
            elif segment == 1:
                messages.append(process_line_message(line))
    print("Loaded, rules:")
    print(rules)
    print("Loaded, messages:")
    print(messages)
    return rules, messages

def process_line_rule(line):
    subrule = None
    rule_id, ruletext = [x.strip() for x in line.split(":")]
    if "\"" in ruletext:
        subrule = ruletext[1]
    else:
        ruleset = [x.strip().split() for x in ruletext.split("|")]
        subrule = []
        for rule in ruleset:
            subrule.append([int(x) for x in rule])
    return int(rule_id), subrule

def process_line_message(line):
    return line.strip()

def apply_rule(rules, message, ruleid, depth=0):
    entire_rule = rules[ruleid]
    print("{}Looking to apply ruleid {}, rule \"{}\" to message {}".format("-"*depth, ruleid, entire_rule, message))
    if len(message) == 0:
        return []
    elif type(entire_rule) == str:
        # 4:"a"
        # 5:"b"
        passed = message[0] == entire_rule
        if passed:
            print("{}It passed the single letter rule \"{}\"".format("-" * depth, entire_rule))
            return [message[1:]]
        else:
            print("{}It did NOT pass the single letter rule \"{}\"".format("-" * depth, entire_rule))
            return []
    else:
        candidate_messages = []
        for ruleset in entire_rule:
            # [1 2] then [3 4]
            valid_messages_for_ruleset = [message]
            # For any possible block of rules, that is, eaither side of the `|`, start the input over.
            for subrule_id in ruleset:
                # 1 then 2
                valid_messages_for_rule = []
                for message_candidate in valid_messages_for_ruleset:
                    valid_messages_for_rule.extend(
                        apply_rule(rules, message_candidate, subrule_id, depth+1)
                    )
                # The next rule can only be applied to successful processes of the previosu rule.
                valid_messages_for_ruleset = valid_messages_for_rule
            candidate_messages.extend(valid_messages_for_ruleset)
        return candidate_messages


def apply_rules(rules, message):
    print("Applying rules to message", message)
    return apply_rule(rules, message, 0)

def part1(input_file):
    ret = 0
    rules, messages = load(input_file)

    # print(sum('' in z(msg, 0, rules) for msg in messages))

    for message in messages:
        passed = '' in apply_rules(rules, message)
        print("Message {} Passed: {}".format(message, passed))
        if passed:
            ret += 1
    return ret

if __name__ == "__main__":
    # p1 = part1("test-small.txt")
    # print(p1)
    # assert p1 == 1

    # p1 = part1("test.txt")
    # print(p1)
    # assert p1 == 2

    p1 = part1("input.txt")
    print(p1)
    assert p1 == 265
    # Not 204.
