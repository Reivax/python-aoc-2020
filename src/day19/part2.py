# NExt time look to https://en.wikipedia.org/wiki/Shunting-yard_algorithm

def load(input_file):
    rules = {}
    messages = []
    segment = 0
    with open(input_file) as f:
        for line in f:
            line = line.strip()
            if len(line) == 0:
                segment += 1
                continue
            if segment == 0:
                ruleid, rule = process_line_rile(line)
                rules[ruleid] = rule
            elif segment == 1:
                messages.append(process_line_message(line))
    print("Loaded, rules:")
    print(rules)
    print("Loaded, messages:")
    print(messages)
    return rules, messages

def process_line_rile(line):
    subrule = None
    rule_id, ruletext = [x.strip() for x in line.split(":")]
    if "\"" in ruletext:
        subrule = ruletext[1]
    elif "|" in ruletext:
        ruleset = [x.strip().split() for x in ruletext.split("|")]
        subrule = []
        for rule in ruleset:
            subrule.append([int(x) for x in rule])
    else:
        subrule = [[int(x.strip()) for x in ruletext.strip().split()]]
    return int(rule_id), subrule

def process_line_message(line):
    return line.strip()

def find_match(rules, message):
    if x in found:
        return list(d[x])
    found.add(x)
    ans = []
    for p in rules[x]:
        if len(p) == 2:
            for a in find(p[0]):
                for b in find(p[1]):
                    ans.append(a + b)
        elif len(p) == 3:
            for a in find(p[0]):
                for b in find(p[1]):
                    for c in find(p[2]):
                        ans.append(a + b + c)
        elif len(p) == 1:
            ans = list(find(p[0]))
    rules[x] = list(ans)
    return list(ans)

def apply_rule(message, ruleid, rules):
    if type(rules[ruleid]) == str:
        if message and message[0] == rules[ruleid]:
            # This is what ultimately causes a zero-length string to return.
            return [message[1:]]
        else:
            return []
    else:
        valid_messages = []
        for ruleset in rules[ruleid]:
            valid_messages_for_ruleset = [message]
            for subrule_id in ruleset:
                valid_messages_for_rule = []
                for message_candidate in valid_messages_for_ruleset:
                    valid_messages_for_rule += apply_rule(message_candidate, subrule_id, rules)
                valid_messages_for_ruleset = valid_messages_for_rule
                if not valid_messages_for_ruleset:
                    break
            if valid_messages_for_ruleset:
                valid_messages += valid_messages_for_ruleset
        return valid_messages


def apply_rules(rules, message):
    print("Applying rules to message", message)
    # return apply_rule(rules, message, 0)
    return apply_rule(message, 0, rules)

def part2(input_file):
    ret = 0
    rules, messages = load(input_file)

    # print(sum('' in z(msg, 0, rules) for msg in messages))

    for message in messages:
        applied = apply_rules(rules, message)
        passed = '' in applied
        # passed = len(applied) > 0
        print("Message {} Passed: {} Applied: {}".format(message, passed, applied))
        if passed:
            ret += 1
    return ret

if __name__ == "__main__":
    # p2 = part2("test-small.txt")
    # print(p2)
    # assert p2 == 1

    # p2 = part2("test.txt")
    # print(p2)
    # assert p2 == 2

    p2 = part2("input2.txt")
    print(p2)
    # Not 204.
