def approach3(input, max_turn):
    # From artcz
    print("Solving ", input, "for", max_turn)
    last_spoken_dict = {}
    for turn, num in enumerate(input, start=1):
        last_spoken_dict[num] = turn

    spoken_now = input[-1]
    while True:
        last_spoken = spoken_now
        spoken_now = (turn - last_spoken_dict[last_spoken]) if last_spoken in last_spoken_dict else 0

        last_spoken_dict[last_spoken]=turn
        turn += 1
        if turn == max_turn:
            print(spoken_now)
            break
    return spoken_now

def approach4(input, max_turn):
    last_spoken_dict = {}
    for turn, num in enumerate(input, start=1):
        last_spoken_dict[num] = turn
    spoken_now = input[-1]
    while turn != max_turn:
        last_spoken = spoken_now
        spoken_now = turn - last_spoken_dict.get(last_spoken, turn)
        last_spoken_dict[last_spoken]=turn
        turn += 1
    print(spoken_now)
    return spoken_now


import typing
def approach5(input: typing.List[int], turn_emit: typing.List[int]):
    ret: typing.List[typing.Optional[int]] = [None] * len(turn_emit)
    last_spoken_dict: typing.Dict = { num: turn for turn, num in enumerate(input, 1)}
    spoken_now: int = input[-1]
    turn: int = len(input)
    mx: int = max(turn_emit)
    while turn <= mx:
        last_spoken = spoken_now
        spoken_now = turn - last_spoken_dict.get(last_spoken, turn)
        last_spoken_dict[last_spoken]=turn
        turn += 1
        if turn in turn_emit:
            ret[turn_emit.index(turn)] = spoken_now
    print(ret)
    return ret


import typing
def approach_generatred(input: typing.List[int], target_turn: int):
    def sequence_generator(initial_state):
        last_spoken_dict: typing.Dict = { num: turn for turn, num in enumerate(initial_state, 1)}
        spoken_now: int = initial_state[-1]
        turn: int = len(initial_state)
        while True:
            last_spoken = spoken_now
            spoken_now = turn - last_spoken_dict.get(last_spoken, turn)
            yield spoken_now
            last_spoken_dict[last_spoken]=turn
            turn += 1
    for turn, value in enumerate(sequence_generator(input), len(input)+1):
        if turn == target_turn:
            return value


if __name__ == "__main__":
    assert approach3([0,3,6], 9) == 4
    assert approach4([0,3,6], 9) == 4
    assert approach_generatred([0,3,6], 9) == 4
    assert approach3([0,3,6], 10) == 0
    assert approach4([0,3,6], 10) == 0
    assert approach_generatred([0,3,6], 10) == 0
    assert approach3([0,3,6], 2020) == 436
    assert approach4([0,3,6], 2020) == 436
    assert approach_generatred([0,3,6], 2020) == 436
    assert approach5([0,3,6], [9, 10, 2020]) == [4, 0, 436]
    assert approach3([5,1,9,18,13,8,0], 2020) == 376
    assert approach4([5,1,9,18,13,8,0], 2020) == 376
    assert approach_generatred([5,1,9,18,13,8,0], 2020) == 376
    # assert approach3([1,3,2], 30000000) == 2578
    # assert approach3([2,1,3], 30000000) == 3544142
    # assert approach3([5,1,9,18,13,8,0], 30000000)
