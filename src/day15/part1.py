test = [0,3,6]
problem_input = [5,1,9,18,13,8,0]

def find_diff_between_last_two_occurences(input, target):
    last = None
    second_last = None
    for idx in range(0, -len(input)):
        idx = -idx
        if input_rev[idx] == target:
            if last is None:
                last = idx
            else:
                second_last = idx
                break
    else:
        return 0
    return abs(last - second_last)

def part1(input, size):
    spoken_numbers = []
    ret = None
    for idx in range(size):
        if not idx%1000:
            print("Turn", idx)
        # print(turn, "Spoken list: ", spoken_numbers)
        if idx < len(input):
            # print(turn, "Opening move", input[idx])
            spoken_numbers.append(input[idx])
            continue
        last_spoken = spoken_numbers[-1]
        # print(turn, "Last spoken", last_spoken)
        last_spoken_count = spoken_numbers.count(last_spoken)
        if last_spoken_count == 1:
            # print(turn, "Now I speak ", 0, "There was only ever one of these before.")
            spoken_numbers.append(0)
            ret = 0
        else:
            diff = find_diff_between_last_two_occurences(spoken_numbers, last_spoken)
            ret = diff
            # print(turn, "Now I speak ", diff)
            spoken_numbers.append(diff)
    print(input, ret)
    return ret



if __name__ == "__main__":
    part1(test, 2020)
    part1(problem_input, 2020) # 376
    # Will not print in a reasonable time.
    # assert part1([0,3,6], 30000000) == 175594
    # assert part1([1,3,2], 30000000) == 2578
    # print(part1(problem_input, 30000000)) # 376
