import copy
from collections import defaultdict
active = '#'
inactive = '.'

def load_linse(input):
    global nx, ny
    board = defaultdict(lambda: inactive)
    for y, row in enumerate(input.split('\n')):
        # print("Loading row", row)
        for x, letter in enumerate(row.strip()):
            # print("Setting z, row, col", y, x, letter)
            if letter == '#':
                board[(0,y,x)] = letter
    nx, ny = x, y
    return board


def count_active_neighbors(cubes, x, y, z):
    total_active = 0
    for dz in [-1, 0, 1]:
        for dy in [-1, 0, 1]:
            for dx in [-1, 0, 1]:
                if dx == dy == dz == 0:
                    continue
                if cubes[(z+dz,y+dy,x+dx)] == active:
                    total_active += 1
    return total_active

def set_state(cubes, x, y, z, state):
    cubes[(z,y,x)] = state

def advance_state(current_cubes, generation):
    # reading_only_cubes = copy.deepcopy(current_cubes)
    next_cubes = defaultdict(lambda: inactive)
    zrange = range(-generation-1, generation+2)
    yrange = range(-generation - 1, ny + generation + 1)
    xrange = range(-generation - 1, nx + generation + 1)

    for z in zrange:
        for y in yrange:
            for x in xrange:
                state = current_cubes[(z,y,x)]
                neighbor_count = count_active_neighbors(current_cubes, x, y, z)
                if x==y==z==0:
                    print("Neighbor count for z, row, col", z, y, x, neighbor_count)
                if state == active:
                    if neighbor_count in (2,3):
                        # print("From A to A")
                        next_cubes[(z,y,x)] = active
                    else:
                        # print("From A to I.")
                        # next_cubes[(z,y,x)] = inactive
                        pass
                elif state == inactive:
                    if neighbor_count == 3:
                        # print("From I to A.")
                        next_cubes[(z,y,x)] = active
                    else:
                        # print("From I to I.")
                        # next_cubes[(z,y,x)] = inactive
                        pass
    return next_cubes

def count_active(cubes):
    total = 0
    for z,y,x in cubes.keys():
        value = cubes[(z,y,x)]
        if value == active:
            total += 1
    print("Located active", total)
    return total

def print_board(board, generation):
    zrange = range(-generation, generation + 1)
    yrange = range(-generation - 1, ny + generation + 1)
    xrange = range(-generation - 1, nx + generation + 1)

    print("Generation", generation)
    for z in zrange:
        print("Z: {}".format(z))
        for y in yrange:
            print("{}: ".format(y), end='')
            for x in xrange:
                print(board[(z,y,x)], end='')
            print()

def quick_print_board(board, geneartion):
    print("Generation", geneartion)
    for ((z,y,x), value) in board.items():
        print("Entry: G{} Z{} Y{} X{} {}".format(geneartion, z, y, x, value))


def part1(input_file, max_turns):
    cubes = load_linse(open(input_file).read())
    # print_board(cubes, 0)
    quick_print_board(cubes, 0)
    for generation in range(1, max_turns+1):
        cubes = advance_state(cubes, generation)
        quick_print_board(cubes, generation)
    # print("Last generation", generation)
    return count_active(cubes)


if __name__ == "__main__":
    # assert part1("test.txt", 0) == 5
    # assert part1("test.txt", 1) == 11
    # assert part1("test.txt", 2) == 21
    # assert part1("test.txt", 3) == 38
    # assert part1("test.txt", 4) == 58
    # assert part1("test.txt", 5) == 101
    # p1 = part1("test.txt", 5)
    # print(p1)
    # assert p1 == 112
    p1 = part1("input.txt", 6)
    print(p1)
    assert p1 == 359
