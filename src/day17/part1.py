
active = '#'
inactive = '.'

puzzle_left = 0
puzzle_right = 0
puzzle_depth = 0

def load_file(input_file):
    with open(input_file) as f:
        lines = load_lines(f.readlines())
    return lines

def load_lines(lines):
    global puzzle_left
    global puzzle_right
    board = {}
    puzzle_left = 0
    puzzle_right = len(lines)
    for lineno, line in enumerate(lines):
        board[lineno] = {}
        for cno, c in enumerate(line.strip()):
            board[lineno][cno] = c
    return board

def get_state(cubes, x, y, z):
    return cubes.get(z, {}).get(y, {}).get(x, inactive)

def set_state(cubes, x, y, z, state):
    if z not in cubes:
        cubes[z] = {}
    if y not in cubes[z]:
        cubes[z][y] = {}
    if x not in cubes[z][y]:
        cubes[z][y][x] = state

def count_active_neighbors(cubes, x, y, z):
    total_active = 0
    for dz in [-1, 0, 1]:
        for dy in [-1, 0, 1]:
            for dx in [-1, 0, 1]:
                if dx == dy == dz == 0:
                    continue
                if get_state(cubes, x+dx, y+dy, z+dz) == active:
                    total_active += 1
    return total_active

def advance_state(cubes):
    ret = {}
    for z in range(-puzzle_depth, puzzle_depth+1):
        for y in range(puzzle_left, puzzle_right):
            for x in range(puzzle_left, puzzle_right):
                # print(single_board.get(row, {}).get(col, inactive), end='')
                state = get_state(cubes, x, y, z)
                neighbor_count = count_active_neighbors(cubes, x, y, z)
                if state == active:
                    if neighbor_count in (2,3):
                        set_state(ret, x, y, z, active)
                    else:
                        set_state(ret, x, y, z, inactive)
                elif state == inactive:
                    if neighbor_count == 3:
                        set_state(ret, x, y, z, active)
                    else:
                        set_state(ret, x, y, z, inactive)
    return ret

def part1(input_file):
    global puzzle_left
    global puzzle_right
    global puzzle_depth
    cubes = {}
    cubes[0] = load_file(input_file)
    print_cube(cubes, 0)
    for generation in range(1, 6):
        cubes = advance_state(cubes)
        puzzle_depth += 1
        print("Generation:", generation)
        for depth in range(-puzzle_depth, puzzle_depth+1):
            print("depth", depth)
            print_cube(cubes, depth)
        puzzle_left -= 1
        puzzle_right += 1
    return len(cubes.keys())

def print_cube(cubes, z):
    single_board = cubes.get(z, {})
    for row in range(puzzle_left, puzzle_right):
        for col in range(puzzle_left, puzzle_right):
            print(single_board.get(row, {}).get(col, '.'), end='')
        print("")



if __name__ == "__main__":
    p1 = part1("test.txt")
    print(p1)
