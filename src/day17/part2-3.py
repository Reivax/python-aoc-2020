from collections import defaultdict
from itertools import product
active = '#'
inactive = '.'

def load_linse(input):
    global nw, nx
    board = defaultdict(lambda: inactive)
    for x, row in enumerate(input.split('\n')):
        print("Loading row", row)
        for w, letter in enumerate(row.strip()):
            print("Setting z, row, col", x, w, letter)
            board[0,0,x,w] = letter
    nw, nx = w, x
    return board


def count_active_neighbors(board, w, x, y, z):
    total_active = 0
    for (dz, dy, dx, dw) in product([-1, 0, 1], repeat=4):
        if dw == dx == dy == dz == 0:
            continue
        if board[(z+dz,y+dy,x+dx,w+dw)] == active:
            total_active += 1
    return total_active

def advance_state(current_board, generation):
    next_board = defaultdict(lambda: inactive)
    zrange = range(-generation-1, generation+2)
    yrange = range(-generation-1, generation+2)
    xrange = range(-generation - 1, nx + generation + 1)
    wrange = range(-generation - 1, nw + generation + 1)

    for (z, y, x, w) in product(zrange, yrange, xrange, wrange):
        state = current_board[(z,y,x,w)]
        neighbor_count = count_active_neighbors(current_board, w, x, y, z)
        if state == active:
            if neighbor_count in (2,3):
                next_board[(z,y,x,w)] = active
        elif state == inactive:
            if neighbor_count == 3:
                next_board[(z,y,x,w)] = active
    return next_board

def count_active(board):
    total = 0
    for z,y,x,w in board.keys():
        value = board[(z,y,x,w)]
        if value == active:
            total += 1
    print("Located active", total)
    return total

def part2(input_file, max_turns):
    board = load_linse(open(input_file).read())
    generation=0
    for generation in range(1, max_turns+1):
        print("generation", generation)
        board = advance_state(board, generation)
    print("Last generation", generation)
    return count_active(board)


if __name__ == "__main__":
    # assert part2("test.txt", 0) == 5
    # assert part2("test.txt", 1) == 29
    # assert part2("test.txt", 2) == 60
    # assert part2("test.txt", 3) == 320
    # print(part2("test.txt", 4))
    assert part2("test.txt", 4) == 188
    # assert part2("test.txt", 5) == 1056
    # assert part2("test.txt", 6) == 848
    # p1 = part1("input.txt")
    # print(p1)
