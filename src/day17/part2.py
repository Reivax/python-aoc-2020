from collections import defaultdict
active = '#'
inactive = '.'

def load_linse(input):
    global nw, nx
    cubes = defaultdict(
        lambda: defaultdict(
            lambda: defaultdict(
                lambda: defaultdict(
                    lambda: inactive
                )
            )
        )
    )
    for x, row in enumerate(input.split('\n')):
        print("Loading row", row)
        for w, letter in enumerate(row.strip()):
            print("Setting z, row, col", x, w, letter)
            cubes[0][0][x][w] = letter

    nw, nx = w, x
    return cubes


def count_active_neighbors(cubes, w, x, y, z):
    total_active = 0
    for dz in [-1, 0, 1]:
        for dy in [-1, 0, 1]:
            for dx in [-1, 0, 1]:
                for dw in [-1, 0, 1]:
                    if dw == dx == dy == dz == 0:
                        continue
                    if cubes[z+dz][y+dy][x+dx][w+dw] == active:
                        total_active += 1
    return total_active

def advance_state(current_cubes, generation):
    next_cubes = defaultdict(
        lambda: defaultdict(
            lambda: defaultdict(
                lambda: defaultdict(
                    lambda: inactive
                )
            )
        )
    )
    zrange = range(-generation-1, generation+2)
    yrange = range(-generation-1, generation+2)
    xrange = range(-generation - 1, nx + generation + 1)
    wrange = range(-generation - 1, nw + generation + 1)

    for z in zrange:
        for y in yrange:
            for x in xrange:
                for w in wrange:
                    state = current_cubes[z][y][x][w]
                    neighbor_count = count_active_neighbors(current_cubes, w, x, y, z)
                    # print("Neighbor count for z,, row, col", z, y, x, neighbor_count)
                    if state == active:
                        if neighbor_count in (2,3):
                            # print("From A to A")
                            next_cubes[z][y][x][w] = active
                        else:
                            # print("From A to I.")
                            # next_cubes[z][y][x][w] = inactive
                            pass
                    elif state == inactive:
                        if neighbor_count == 3:
                            # print("From I to A.")
                            next_cubes[z][y][x][w] = active
                        else:
                            # print("From I to I.")
                            # next_cubes[z][y][x][w] = inactive
                            pass
    return next_cubes

def count_active(cubes):
    total = 0
    for z in cubes.keys():
        for y in cubes[z].keys():
            for x in cubes[z][y].keys():
                for w in cubes[z][y][x].keys():
                    value = cubes[z][y][x][w]
                    if value == active:
                        total += 1
    print("Located active", total)
    return total

def part2(input_file):
    cubes = load_linse(open(input_file).read())
    # print_board(cubes, 0, 3)
    for generation in range(1, 7):
        print("generation", generation)
        cubes = advance_state(cubes, generation)
        # print_board(cubes, generation, 3)
    print("Last generation", generation)
    return count_active(cubes)


if __name__ == "__main__":
    p2 = part2("test.txt")
    print(p2)
    assert p2 == 848
    # p1 = part1("input.txt")
    # print(p1)
